export const SUBSCRIBE_TO_PLAN_REQUEST = 'SUBSCRIBE_TO_PLAN_REQUEST';
export const SUBSCRIBE_TO_PLAN_SUCCESS = 'SUBSCRIBE_TO_PLAN_SUCCESS';
export const SUBSCRIBE_TO_PLAN_FAIL = 'SUBSCRIBE_TO_PLAN_FAIL';

export const CREATE_MEMBERSHIP_REQUEST = 'CREATE_MEMBERSHIP_REQUEST';
export const CREATE_MEMBERSHIP_SUCCESS = 'CREATE_MEMBERSHIP_SUCCESS';
export const CREATE_MEMBERSHIP_FAIL = 'CREATE_MEMBERSHIP_FAIL';

export const UPDATE_MEMBERSHIP_REQUEST = 'UPDATE_MEMBERSHIP_REQUEST';
export const UPDATE_MEMBERSHIP_SUCCESS = 'UPDATE_MEMBERSHIP_SUCCESS';
export const UPDATE_MEMBERSHIP_FAIL = 'UPDATE_MEMBERSHIP_FAIL';

export const GET_MEMBERSHIP_REQUEST = 'GET_MEMBERSHIP_REQUEST';
export const GET_MEMBERSHIP_SUCCESS = 'GET_MEMBERSHIP_SUCCESS';
export const GET_MEMBERSHIP_FAIL = 'GET_MEMBERSHIP_FAIL';

export const GET_ACTIVE_MEMBERSHIP_REQUEST = 'GET_ACTIVE_MEMBERSHIP_REQUEST';
export const GET_ACTIVE_MEMBERSHIP_SUCCESS = 'GET_ACTIVE_MEMBERSHIP_SUCCESS';
export const GET_ACTIVE_MEMBERSHIP_FAIL = 'GET_ACTIVE_MEMBERSHIP_FAIL';

export const GET_PLAN_REQUEST = 'GET_PLAN_REQUEST';
export const GET_PLAN_SUCCESS = 'GET_PLAN_SUCCESS';
export const GET_PLAN_FAIL = 'GET_PLAN_FAIL';

export const REMOVE_MEMBERSHIP_REQUEST = 'REMOVE_MEMBERSHIP_REQUEST';
export const REMOVE_MEMBERSHIP_SUCCESS = 'REMOVE_MEMBERSHIP_SUCCESS';
export const REMOVE_MEMBERSHIP_FAIL = 'REMOVE_MEMBERSHIP_FAIL';