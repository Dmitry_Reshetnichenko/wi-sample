export const FETCH_SCOPES_REQUEST = 'FETCH_SCOPES_REQUEST';
export const FETCH_SCOPES_SUCCESS = 'FETCH_SCOPES_SUCCESS';
export const FETCH_SCOPES_FAIL = 'FETCH_SCOPES_FAIL';

export const FETCH_SECTIONS_REQUEST = 'FETCH_SECTIONS_REQUEST';
export const FETCH_SECTIONS_SUCCESS = 'FETCH_SECTIONS_SUCCESS';
export const FETCH_SECTIONS_FAIL = 'FETCH_SECTIONS_FAIL';

export const FETCH_CATEGORIES_REQUEST = 'FETCH_CATEGORIES_REQUEST';
export const FETCH_CATEGORIES_SUCCESS = 'FETCH_CATEGORIES_SUCCESS';
export const FETCH_CATEGORIES_FAIL = 'FETCH_CATEGORIES_FAIL';

export const FETCH_ADS_REQUEST = 'FETCH_ADS_REQUEST';
export const FETCH_ADS_SUCCESS = 'FETCH_ADS_SUCCESS';
export const FETCH_ADS_FAIL = 'FETCH_ADS_FAIL';

export const FETCH_PLANS_REQUEST = 'FETCH_PLANS_REQUEST';
export const FETCH_PLANS_SUCCESS = 'FETCH_PLANS_SUCCESS';
export const FETCH_PLANS_FAIL = 'FETCH_PLANS_FAIL';

export const FETCH_CURRENCIES_REQUEST = 'FETCH_CURRENCIES_REQUEST';
export const FETCH_CURRENCIES_SUCCESS = 'FETCH_CURRENCIES_SUCCESS';
export const FETCH_CURRENCIES_FAIL = 'FETCH_CURRENCIES_FAIL';

export const FETCH_COUNTRIES_REQUEST = 'FETCH_COUNTRIES_REQUEST';
export const FETCH_COUNTRIES_SUCCESS = 'FETCH_COUNTRIES_SUCCESS';
export const FETCH_COUNTRIES_FAIL = 'FETCH_COUNTRIES_FAIL';

export const FETCH_SYSTEM_CONFIGURATION_REQUEST = 'FETCH_SYSTEM_CONFIGURATION_REQUEST';
export const FETCH_SYSTEM_CONFIGURATION_SUCCESS = 'FETCH_SYSTEM_CONFIGURATION_SUCCESS';
export const FETCH_SYSTEM_CONFIGURATION_FAIL = 'FETCH_SYSTEM_CONFIGURATION_FAIL';

export const PLANS_MODAL_SHOW = 'PLANS_MODAL_SHOW';
export const PLANS_MODAL_HIDE = 'PLANS_MODAL_HIDE';

export const ACHIEVEMENT_MODAL_SHOW = 'ACHIEVEMENT_MODAL_SHOW';
export const ACHIEVEMENT_MODAL_HIDE = 'ACHIEVEMENT_MODAL_HIDE';

export const NEXT__ACHIEVEMENT_MODAL = 'NEXT__ACHIEVEMENT_MODAL';
export const PREV__ACHIEVEMENT_MODAL = 'PREV__ACHIEVEMENT_MODAL';


