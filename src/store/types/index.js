import * as products from './products';
import * as auth from './auth';
import * as authors from './authors';
import * as sidebarFilter from './sidebarFilter';
import * as global from './global';
import * as document from './document';
import * as sidebarProducts from './sidebarProducts';
import * as comments from './comments';
import * as alert from './alert';
import * as plans from './plans';
import * as calendar from './calendar';
import * as basket from './basket';
import * as profile from './profile';
import * as polls from './polls';
import * as breadcrumbs from './breadcrumbs';
import * as payment from './payment';
import * as likes from './likes';
import * as achievements from './achievements';
import * as chakras from './chakras';
import * as onboard from './onboard';
import * as notifications from './notifications';

export default {
	auth,
  notifications,
  likes,
	authors,
	products,
	sidebarFilter,
	global,
	document,
	sidebarProducts,
	comments,
	alert,
	plans,
	calendar,
  basket,
	profile,
	polls,
	breadcrumbs,
	payment,
	achievements,
	chakras,
	onboard
};