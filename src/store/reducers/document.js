import types from '../types';

const initDocument = {
    _id: null,
    image: {
        thumb_web: {},
        full: {}
    },
    video: {
        video_full: {},
        video_short: {}
    },
    content: {},
    author: {
        regalia: ''
    },
    topics: [],
    items: [],
    title: ''
};
const initialState = {
    document: initDocument,
    loading: false,
    accessPlans: []
};

const documentReducer = (state = initialState, action) => {
    switch(action.type) {

        case types.document.GET_DOCUMENT_REQUEST:
            return { ...state, loading: true, document: initDocument };
        case types.document.GET_DOCUMENT_SUCCESS:
            return { ...state, document: action.data.productsFiltersV2.data[0], loading: false };
        case types.document.GET_DOCUMENT_FAIL:
            return { ...state, loading: false };
            
        case types.document.GET_PLANS_BY_PRODUCT_REQUEST:
            return { ...state, loading: true};
        case types.document.GET_PLANS_BY_PRODUCT_SUCCESS:
            return { ...state, accessPlans: action.data.PlansByProduct, loading: false };
        case types.document.GET_PLANS_BY_PRODUCT_FAIL:
            return { ...state, loading: false };

        default:
            return state;
    }
};

export default documentReducer;
