import types from '../types';

const PRODUCTS_PER_PAGE = 20;

const initialState = {
  products: [],
  pagination: {
    page: 0,
    perPage: PRODUCTS_PER_PAGE
  },
  hasMore: false,
  loading: false
};

const sidebarProducts = (state = initialState, action) => {
  switch (action.type) {

    case types.sidebarProducts.LOAD_MORE_SIDEBAR_PRODUCTS_REQUEST:
      return {
        ...state,
        loading: true,
        pagination: {
          ...state.pagination,
          page: ++state.pagination.page
        },
      };
    case types.sidebarProducts.LOAD_MORE_SIDEBAR_PRODUCTS_SUCCESS:
      return {
        ...state,
        products: [...state.products, ...action.data.productsFiltersV2.data],
        hasMore: action.data.productsFiltersV2.has_more,
        loading: false
      };
    case types.sidebarProducts.LOAD_MORE_SIDEBAR_PRODUCTS_FAIL:
      return {...state, loading: false};


    case types.sidebarProducts.CLEAR_SIDEBAR_PRODUCTS:
      return {...state, products: [], pagination: {...state.pagination, page: 0}, hasMore: false};

    default:
      return state;
  }
};

export default sidebarProducts;
