import types from '../types';
import SSRHelper from './../../helpers/SSRHelper';
import AnalyticsHelper from './../../helpers/AnalyticsHelper';

const initialState = {
  scopes: [],
  sections: [],
  categories: [],
  plans: [],
  currencies: [],
  countries: [],
  planModalShow: false,
  ads: [],
  loading: false,
  isSSR: SSRHelper.isSSR(),
  systemConfiguration: null
};

const articlesReducer = (state = initialState, action) => {

  switch (action.type) {

    case types.global.FETCH_SCOPES_REQUEST:
      return {...state, loading: true};
    case types.global.FETCH_SCOPES_SUCCESS:
      return {...state, scopes: action.data.ScopeAll, loading: false};
    case types.global.FETCH_SCOPES_FAIL:
      return {...state, loading: false};

    case types.global.FETCH_SECTIONS_REQUEST:
      return {...state, loading: true};
    case types.global.FETCH_SECTIONS_SUCCESS:
      return {...state, sections: action.data.SectionAll, loading: false};
    case types.global.FETCH_SECTIONS_FAIL:
      return {...state, loading: false};

    case types.global.FETCH_CATEGORIES_REQUEST:
      return {...state, loading: true};
    case types.global.FETCH_CATEGORIES_SUCCESS:
      return {...state, categories: action.data.TopicAll, loading: false};
    case types.global.FETCH_CATEGORIES_FAIL:
      return {...state, loading: false};

    case types.global.FETCH_PLANS_REQUEST:
      return {...state, loading: true};
    case types.global.FETCH_PLANS_SUCCESS:
      return {...state, plans: action.data.PlanAll, loading: false};
    case types.global.FETCH_PLANS_FAIL:
      return {...state, loading: false};

    case types.global.FETCH_COUNTRIES_REQUEST:
      return {...state, loading: true};
    case types.global.FETCH_COUNTRIES_SUCCESS:
      return {...state, countries: action.data.CountryAll, loading: false};
    case types.global.FETCH_COUNTRIES_FAIL:
      return {...state, loading: false};

    case types.global.FETCH_CURRENCIES_REQUEST:
      return {...state, loading: true};
    case types.global.FETCH_CURRENCIES_SUCCESS:
      return {...state, currencies: action.data.CurrencyAll, loading: false};
    case types.global.FETCH_CURRENCIES_FAIL:
      return {...state, loading: false};

    case types.global.FETCH_ADS_REQUEST:
      return {...state, loading: true};
    case types.global.FETCH_ADS_SUCCESS:
      return {...state, ads: action.data.productsFiltersV2.data, loading: false};
    case types.global.FETCH_ADS_FAIL:
      return {...state, loading: false};

    case types.global.FETCH_SYSTEM_CONFIGURATION_REQUEST:
      return {...state, loading: true};
    case types.global.FETCH_SYSTEM_CONFIGURATION_SUCCESS:
      return {...state, systemConfiguration: action.data.ConfigOne.config_data, loading: false};
    case types.global.FETCH_SYSTEM_CONFIGURATION_FAIL:
      return {...state, systemConfiguration: { maintenance_mode: true }, loading: false};

    case types.global.PLANS_MODAL_SHOW: {
      AnalyticsHelper.eventViewPaymentPage();
      return {...state, planModalShow: true};
    }
    case types.global.PLANS_MODAL_HIDE:
      return {...state, planModalShow: false};

    default:
      return state;
  }
};

export default articlesReducer;
