import types from '../types';
import SSRHelper from './../../helpers/SSRHelper';

const initialState = {
  likes: []
};

const likesReducer = (state = initialState, action) => {

  switch (action.type) {

    case types.likes.GET_LIKES_REQUEST:
      return {...state, loading: true};
    case types.likes.GET_LIKES_SUCCESS:
      return {...state, likes: action.data.LikePagination, loading: false};
    case types.likes.GET_LIKES_FAIL:
      return {...state, loading: false};


    default:
      return state;
  }
};

export default likesReducer;