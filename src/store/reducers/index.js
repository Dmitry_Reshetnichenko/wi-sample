import { combineReducers } from 'redux';
import auth from './auth';
import authors from './authors';
import products from './products';
import sidebarFilter from './sidebarFilter';
import sidebarProducts from './sidebarProducts';
import global from './global';
import document from './document';
import calendar from './calendar';
import basket from './basket';
import comments from './comments';
import alert from './alert';
import plans from './plans';
import profile from './profile';
import polls from './polls';
import breadcrumbs from './breadcrumbs';
import payment from './payment';
import modals from './modals';
import likes from './likes';
import achievements from './achievements';
import chakras from './chakras';
import onboard from './onboard';
import notifications from './notifications';

export default combineReducers({
	auth,
  authors,
  notifications,
  likes,
	products,
	sidebarFilter,
	sidebarProducts,
	global,
	document,
	comments,
	alert,
	plans,
	calendar,
	basket,
	profile,
	polls,
  modals,
	breadcrumbs,
	payment,
	achievements,
	chakras,
	onboard
})