import types from '../types';

const initialState = {
  notifications: [],
  notificationsCount: null,
  unreadNotifications: 0,
  pageInfo: {
    currPage: 0
  },
  loading: true,
};

const notificationsReducer = (state = initialState, action) => {

  switch (action.type) {

    case types.notifications.GET_NOTIFICATIONS_REQUEST:
      return {...state, loading: true};
    case types.notifications.GET_NOTIFICATIONS_SUCCESS:
      return {
        ...state,
        notifications: action.data.NotificationPagination.items,
        notificationsCount: action.data.NotificationPagination.count,
        pageInfo: action.data.NotificationPagination.pageInfo,
        loading: false
      };
    case types.notifications.GET_NOTIFICATIONS_FAIL:
      return {...state, loading: false};

    case types.notifications.GET_NEW_NOTIFICATIONS_COUNT_REQUEST:
      return {...state, loading: true};
    case types.notifications.GET_NEW_NOTIFICATIONS_COUNT_SUCCESS:
      return {
        ...state,
        unreadNotifications: action.data.NotificationPagination.count,
        loading: false
      };
    case types.notifications.GET_NEW_NOTIFICATIONS_COUNT_FAIL:
      return {...state, loading: false};

    case types.notifications.LOAD_MORE_NOTIFICATION_MANY_NOTIFICATIONS_REQUEST:
      return {...state, loading: true};
    case types.notifications.LOAD_MORE_NOTIFICATION_MANY_NOTIFICATIONS_SUCCESS:
      return {
        ...state,
        notifications: [...state.notifications, ...action.data.NotificationPagination.items],
        notificationsCount: action.data.NotificationPagination.count,
        pageInfo: action.data.NotificationPagination.pageInfo,
        loading: false
      };
    case types.notifications.LOAD_MORE_NOTIFICATION_MANY_NOTIFICATIONS_FAIL:
      return {...state, loading: false};

    default:
      return state;
  }
};

export default notificationsReducer;