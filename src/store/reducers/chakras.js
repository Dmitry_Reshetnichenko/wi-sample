import types from '../types';
import DataHelper from '../../helpers/DataHelper';

const initialState = {
  loading: false,
  chakraAll: [],
  chakraResults: [],
  chakraTasks: null,
  quizData: [],
  chartsData: null,
};

const chakrasReducer = (state = initialState, action) => {
  switch (action.type) {


    case types.chakras.FETCH_CHAKRAS_REQUEST:
      return {...state, loading: true};
    case types.chakras.FETCH_CHAKRAS_SUCCESS:
      return {
        ...state,
        chakraAll: action.data.ChakraAll,
        quizData: DataHelper.getQuizInformation(action.data.ChakraAll),
        loading: false
      };
    case types.chakras.FETCH_CHAKRAS_FAIL:
      return {...state, loading: false};

    case types.chakras.FETCH_CHARTS_DATA_REQUEST:
      return {...state, loading: true};
    case types.chakras.FETCH_CHARTS_DATA_SUCCESS:
      return {
        ...state,
        chartsData: action.data.ChakraStatistic,
        loading: false
      };
    case types.chakras.FETCH_CHARTS_DATA_FAIL:
      return {...state, loading: false};

    case types.chakras.SET_ACTIVE_QUIZ_ANSWERS: {
      const {quizData} = state;
      const newQuizData = [...quizData];
      newQuizData[action.parentIndex].answers.forEach((item) => {
        item.isActive = false;
      });
      newQuizData[action.parentIndex].answers[action.chaildIndex].isActive = true;
      return {
        quizData: newQuizData,
        ...state
      };
    }

    case types.chakras.RECORD_QUIZ_REQUEST:
      return {...state, loading: true};
    case types.chakras.RECORD_QUIZ_SUCCESS:
      return {...state, loading: false};
    case types.chakras.RECORD_QUIZ__FAIL:
      return {...state, loading: false};

    case types.chakras.FETCH_CHAKRA_TASKS_REQUEST:
      return {...state, loading: true};
    case types.chakras.FETCH_CHAKRA_TASKS_SUCCESS:
      return {...state, chakraTasks: action.data.ChakraCurrentTasks, loading: false};
    case types.chakras.FETCH_CHAKRA_TASKS_FAIL:
      return {...state, loading: false};

    case types.chakras.GET_RESULTS_REQUEST:
      return {...state, loading: true};
    case types.chakras.GET_RESULTS_SUCCESS:
      return {...state, loading: false, chakraResults: action.data.ChakraLevel};
    case types.chakras.GET_RESULTS_FAIL:
      return {...state, loading: false};

    default:
      return state;
  }
  ;
};

export default chakrasReducer;
