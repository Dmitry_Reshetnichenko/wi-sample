import types from '../types';

const initialState = {
	show: false,
  description: '',
  title: '',
  onAccept: () => {}
};

const alertReducer = (state = initialState, action) => {
    switch(action.type) {

        case types.alert.SHOW_ALERT:
            return { ...state, show: true, ...action.payload, onAccept: action.payload.onAccept || (() => {}) };

        case types.alert.HIDE_ALERT:
            return { ...state, show: false };


        default:
            return state;
    }
};

export default alertReducer;