import types from '../types';

const initialState = {
  polls:[],
  pollStats:[],
  myAnswer:[],
  loading: false
};

const pollsReducer = (state = initialState, action) => {
  switch(action.type) {

    case types.polls.LOAD_POLLS_REQUEST:
      return { ...state, loading: true };
    case types.polls.LOAD_POLLS_SUCCESS:
      return { ...state, loading: false };
    case types.polls.LOAD_POLLS_FAIL:
      return { ...state, loading: false};


    case types.polls.GET_POLLS_REQUEST:
      return { ...state, loading: true};
    case types.polls.GET_POLLS_SUCCESS:
      return { ...state, polls: action.data.PollAll, loading: false };
    case types.polls.GET_POLLS_FAIL:
      return { ...state, loading: false};

    case types.polls.GET_POLLS_CHECK_ANSWER_REQUEST:
      return { ...state, loading: true};
    case types.polls.GET_POLLS_CHECK_ANSWER_SUCCESS:
      return { ...state, myAnswer: action.data.Poll_responseMany, loading: false };
    case types.polls.GET_POLLS_CHECK_ANSWER_FAIL:
      return { ...state, loading: false};

      case types.polls.CLEAR_MY_POLL_ANSWER:
      return { ...state, myAnswer: []};

      case types.polls.CLEAR_POLL_STATS:
      return { ...state, pollStats: []};

    case types.polls.CREATE_POLL_REQUEST:
      return { ...state, loading: true};
    case types.polls.CREATE_POLL_SUCCESS:
      return { ...state, loading: false };
    case types.polls.CREATE_POLL_FAIL:
      return { ...state, loading: false};

      case types.polls.EDIT_POLL_QUESTION_ANSWER_REQUEST:
      return { ...state, loading: true};
    case types.polls.EDIT_POLL_QUESTION_ANSWER_SUCCESS:
      return { ...state, loading: false };
    case types.polls.EDIT_POLL_QUESTION_ANSWER_FAIL:
      return { ...state, loading: false};

    case types.polls.CREATE_POLL_QUESTION_ANSWER_REQUEST:
      return { ...state, loading: true};
    case types.polls.CREATE_POLL_QUESTION_ANSWER_SUCCESS:
      return { ...state, loading: false };
    case types.polls.CREATE_POLL_QUESTION_ANSWER_FAIL:
      return { ...state, loading: false};

    case types.polls.GET_POLLS_STATS_REQUEST:
      return { ...state, loading: true};
    case types.polls.GET_POLLS_STATS_SUCCESS:
      return { ...state, pollStats: action.data.PollStats, loading: false };
    case types.polls.GET_POLLS_STATS_FAIL:
      return { ...state, loading: false};


    default:
      return state;
  }
};

export default pollsReducer;