import 'isomorphic-fetch';
import rootStore from './reducers'
import { applyMiddleware, createStore } from 'redux';
import { logger } from 'redux-logger'
import axios from 'axios';
import axiosMiddleware from 'redux-axios-middleware';
import apolloGraphMiddleware from './apolloGraphMiddleware';
import ApolloClient from 'apollo-boost';
import AuthHelper from '../helpers/AuthHelper';

let axiosConfig = {
  baseURL: process.env.REST_API_URL,
  responseType: 'json',
  headers: {}
};
const axiosMiddlewareConfig = {
  interceptors: {
    response: [
      ({ getState }, res) => {
        if(res.config.reduxSourceAction.types[0] === 'LOGIN_REQUEST')
          res.data.plans = getState().global.plans;
        return Promise.resolve(res)
      }
    ]
  }
};
let graphConfig = {
  uri: process.env.GRAPH_API_URL,
  headers: {}
};


if(AuthHelper.tokeIsDefined()) {
  const token = AuthHelper.getToken();
  axiosConfig.headers['Authorization'] = `JWT ${token}`;
  graphConfig.headers['Authorization'] = `JWT ${token}`;
}

const savePreloadedState = ({ getState }) => next => action => {
  const returnValue = next(action);
  window.__PRELOADED_STATE__ = {...window.__PRELOADED_STATE__, ...getState()};//deepmerge(window.__PRELOADED_STATE__ || {}, getState());
  return returnValue;
};

const queueMiddleware = ({ dispatch, getState }) => next => action => {
  if(action.type === 'QUEUE')
    for(let act of action.actions)
      dispatch(act(getState()));
  next(action);
};

const createApolloClient = () => {
  const client = new ApolloClient(graphConfig);
  return client;
}

export default createStore(rootStore,window.__PRELOADED_STATE__ || {}, applyMiddleware(...[
  queueMiddleware,
  axiosMiddleware(axios.create(axiosConfig), axiosMiddlewareConfig),
  apolloGraphMiddleware(createApolloClient()),
  process.env.NODE_ENV === 'development' && logger,
  savePreloadedState
].filter(Boolean)));
