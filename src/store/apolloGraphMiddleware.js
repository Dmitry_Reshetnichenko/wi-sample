import { gql } from "apollo-boost";

export default graphClient => store => next => action => {
  if(action.type === 'GRAPH') {
    const [ REQUEST, SUCCESS, FAIL ] = action.types;
    store.dispatch({ type: REQUEST, meta: action.meta });
    const mutation = Boolean(action.data.query.trim().match(/^mutation/));
    const queryType = mutation ? 'mutation' : 'query';
    let graphOptions = { [queryType]: gql(action.data.query), fetchPolicy: 'no-cache' };
    if(action.data.variables)
      graphOptions.variables = action.data.variables;

    graphClient[mutation ? 'mutate' : 'query'](graphOptions)
      .then(result => {
        store.dispatch({ type: SUCCESS, data: result.data });
        if(action.onSuccess){
          if(!Array.isArray(action.onSuccess))
            action.onSuccess = [action.onSuccess];
          for(let act of action.onSuccess) {
            const success = act(result.data, store.getState());
            if(success && success.type)
              store.dispatch(success);
          }
        }
      })
      .catch(err => {
        store.dispatch({ type: FAIL, data: err });
        if(action.onFail){
          if(!Array.isArray(action.onFail))
            action.onFail = [action.onFail];
          for(let act of action.onFail){
            const fail = act(result.data, store.getState());
            if(fail && fail.type)
              store.dispatch(fail);
          }
        }
      });
  }
  next(action);
};