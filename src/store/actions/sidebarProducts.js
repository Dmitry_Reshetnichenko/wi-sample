import types from '../types';
import store from "../index";
import graphQueries from '../graphQueries';

export const loadMoreProducts = filters => ({
  type: 'GRAPH',
  types: [types.sidebarProducts.LOAD_MORE_SIDEBAR_PRODUCTS_REQUEST, types.sidebarProducts.LOAD_MORE_SIDEBAR_PRODUCTS_SUCCESS, types.sidebarProducts.LOAD_MORE_SIDEBAR_PRODUCTS_FAIL],
  data: {
    query: graphQueries.products.getProducts(),
    variables: {
      filters: {...filters, is_active: true}
    }
  }
});

export const clearProducts = () => ({
  type: types.sidebarProducts.CLEAR_SIDEBAR_PRODUCTS
});

export const refreshProducts = (filters, limit) => ({
  type: 'QUEUE',
  actions: [
    () => clearProducts(),
    state => loadMoreProducts({
      ...filters,
      pagination: {
        limit: limit || state.sidebarProducts.pagination.perPage,
        offset: state.sidebarProducts.pagination.page * state.sidebarProducts.pagination.perPage
      }
    })
  ]
});


