import amplitude from 'amplitude-js';
import types from '../types';
import * as TabTypes from '../../components/modals/AuthModal/TabTypes';
import graphQueries from '../graphQueries';
import { fetchAds } from './global';
import { getMemberships } from './plans';
import { updateUser } from './profile';
import { showAlert } from './alert';
import store from '../index';
import _ from 'lodash';

export const login = (credentials, saleForm) => ({
		types: [ types.auth.LOGIN_REQUEST, types.auth.LOGIN_SUCCESS, types.auth.LOGIN_FAIL ],
			payload: {
			request: {
				url: '/auth/user/login',
					method: 'POST',
					data: credentials,
			},
			options: {
				onSuccess: ({ dispatch, response }) => {
					dispatch({ type: types.auth.LOGIN_SUCCESS, payload: response.data});
					saleForm && localStorage.setItem('BY_PLAN', 'true')
				}
			}
		}
	});

export const register = (data, saleForm) => ({
  types: [ types.auth.REGISTER_REQUEST, types.auth.REGISTER_SUCCESS, types.auth.REGISTER_FAIL ],
	payload: {
  	request: {
			url: '/auth/user/register',
			method: 'POST',
			data
		},
    options: {
      onSuccess: ({ dispatch, response }) => {
        dispatch({ type: types.auth.REGISTER_SUCCESS, payload: response.data});
        saleForm && localStorage.setItem('BY_PLAN', 'true')
      }
    }
	}
});

export const logout = () => ({
	type: types.auth.LOGOUT,
	payload: {
		plans: store.getState().global.plans,
		user: store.getState().auth.user,
	}
});

export const openModal = tab => ({
	type: types.auth.OPEN_AUTH_MODAL,
	payload: tab || TabTypes.LOGIN
});

export const selectModalTab = tab => ({
	type: types.auth.SELECT_AUTH_MODAL_TAB,
	payload: tab || TabTypes.LOGIN
});

export const closeModal = () => ({
	type: types.auth.CLOSE_AUTH_MODAL
});

export const impersonate = token => ({
	type: types.auth.IMPERSONATE,
	payload: { token }
});


export const definePremiumStatus = userId => ({
	type: 'GRAPH',
	types: [types.profile.DEFINE_PREMIUM_STATUS_REQUEST, types.profile.DEFINE_PREMIUM_STATUS_SUCCESS, types.profile.DEFINE_PREMIUM_STATUS_FAIL],
	data: {
		query: graphQueries.profile.definePremiumStatus(),
		variables: { userId }
	},
});

export const verifyToken = () => ({
	types: [ types.auth.VERIFY_TOKEN_REQUEST, types.auth.VERIFY_TOKEN_SUCCESS, types.auth.VERIFY_TOKEN_FAIL ],
	payload: {
		request: {
			url: 'auth/user/jwt-verify',
			method: 'GET'
		},
		options: {
  		onSuccess: ({ dispatch }) => {
  			dispatch({ type: types.auth.VERIFY_TOKEN_SUCCESS });
				dispatch(fetchUser());
			},
			onError: ({ dispatch }) => {
  			dispatch({ type: types.auth.VERIFY_TOKEN_FAIL });
  			let modalTab = (new URLSearchParams(location.search)).get('modalTab');
      	if(Object.values(TabTypes).includes(modalTab))
        	dispatch(openModal(modalTab));
			}
		}
	}
});

export const forgotPassword = email => ({
	type: 'GRAPH',
	types: [types.auth.FORGOT_PASSWORD_REQUEST, types.auth.FORGOT_PASSWORD_SUCCESS, types.auth.FORGOT_PASSWORD_FAIL],
	data: {
		query: graphQueries.auth.forgotPassword(),
		variables: { email }
	}
});

export const resetPassword = ({ token, password }) => ({
	type: 'GRAPH',
	types: [types.auth.RESET_PASSWORD_REQUEST, types.auth.RESET_PASSWORD_SUCCESS, types.auth.RESET_PASSWORD_FAIL],
	data: {
		query: graphQueries.auth.resetPassword(),
		variables: { token, password }
	}
});

export const changePassword = ({ userId, password }) => ({
	type: 'GRAPH',
	types: [types.auth.CHANGE_PASSWORD_REQUEST, types.auth.CHANGE_PASSWORD_SUCCESS, types.auth.CHANGE_PASSWORD_FAIL],
	data: {
		query: graphQueries.auth.changePassword(),
		variables: { userId, password }
	},
	onSuccess: () =>
		showAlert({
			title: 'Изменение пароля',
			description: 'Ваш пароль успешно изменен',
		})
});
