import types from '../types';
import graphQueries from '../graphQueries';
import { fetchDocument } from './document';
import store from '../index';

export const getLikes = (filter) => ({
  type: 'GRAPH',
  types: [types.likes.GET_LIKES_REQUEST, types.likes.GET_LIKES_SUCCESS, types.likes.GET_LIKES_FAIL],
  data: {
    query: graphQueries.likes.getLikes(),
    variables: { filter }
  },
});

export const likeDocument = like => ({
  type: 'GRAPH',
  types: [types.likes.CREATE_LIKE_REQUEST, types.likes.CREATE_LIKE_SUCCESS, types.likes.CREATE_LIKE_FAIL],
  data: {
    query: graphQueries.likes.likeCreateOne(),
    variables: { like }
  },
  onSuccess: () => fetchDocument(like.entry_id)
});

export const removeManyLikes = (likeFilter, filter) => ({
  type: 'GRAPH',
  types: [types.likes.REMOVE_LIKE_REQUEST, types.likes.REMOVE_LIKE_SUCCESS, types.likes.REMOVE_LIKE_FAIL],
  data: {
    query: graphQueries.likes.removeManyLikes(),
    variables: { likeFilter }
  },
  onSuccess: () => getLikes(filter)
});
