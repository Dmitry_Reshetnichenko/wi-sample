import types from '../types';
import graphQueries from '../graphQueries';
import store from '../index';

export const getNotifications = (filter, setPagination) => {
  const pagination = setPagination || store.getState().notifications.pageInfo.currPage;
  return {
    type: 'GRAPH',
    types: [types.notifications.GET_NOTIFICATIONS_REQUEST, types.notifications.GET_NOTIFICATIONS_SUCCESS, types.notifications.GET_NOTIFICATIONS_FAIL],
    data: {
      query: graphQueries.notification.getNotifications(),
      variables: {...filter, ...pagination}
    }
  }
};

export const getNewNotificationsCount = (filter) => ({
  type: 'GRAPH',
  types: [types.notifications.GET_NEW_NOTIFICATIONS_COUNT_REQUEST, types.notifications.GET_NEW_NOTIFICATIONS_COUNT_SUCCESS, types.notifications.GET_NEW_NOTIFICATIONS_COUNT_FAIL],
  data: {
    query: graphQueries.notification.getNotifications(),
    variables: filter
  }
});

export const updateNotificationById = (record) => ({
  type: 'GRAPH',
  types: [types.notifications.UPDATE_NOTIFICATION_BY_ID_NOTIFICATIONS_REQUEST, types.notifications.UPDATE_NOTIFICATION_BY_ID_NOTIFICATIONS_SUCCESS, types.notifications.UPDATE_NOTIFICATION_BY_ID_NOTIFICATIONS_FAIL],
  data: {
    query: graphQueries.notification.updateNotificationById(),
    variables: record
  },
  onSuccess: [
    () => getNotifications({filter: {user: store.getState().auth.user._id}}),
    () => getNewNotificationsCount({filter: {user: store.getState().auth.user._id, is_read: false}}),
  ]
});

export const loadMoreNotification = (filter, setPagination) => {
  const pagination = setPagination || store.getState().notifications.pageInfo.currentPage;
  return {
    type: 'GRAPH',
    types: [types.notifications.LOAD_MORE_NOTIFICATION_MANY_NOTIFICATIONS_REQUEST, types.notifications.LOAD_MORE_NOTIFICATION_MANY_NOTIFICATIONS_SUCCESS, types.notifications.LOAD_MORE_NOTIFICATION_MANY_NOTIFICATIONS_FAIL],
    data: {
      query: graphQueries.notification.getNotifications(),
      variables: {filter: {user: store.getState().auth.user._id}, page: pagination + 1}
    }
  }
};

export const updateNotificationMany = (record, filter) => ({
  type: 'GRAPH',
  types: [types.notifications.UPDATE_NOTIFICATION_MANY_NOTIFICATIONS_REQUEST, types.notifications.UPDATE_NOTIFICATION_MANY_NOTIFICATIONS_SUCCESS, types.notifications.UPDATE_NOTIFICATION_MANY_NOTIFICATIONS_FAIL],
  data: {
    query: graphQueries.notification.updateNotificationMany(),
    variables: {record, filter}
  },
  onSuccess: [
    () => getNotifications({filter: {user: store.getState().auth.user._id}}),
    () => getNewNotificationsCount({filter: {user: store.getState().auth.user._id, is_read: false}})
  ]

});