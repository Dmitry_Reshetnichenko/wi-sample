import types from '../types';
import graphQueries from '../graphQueries';
import store from '../index';
import AnalyticsHelper from '../../helpers/AnalyticsHelper';

export const fetchDocument = productId => {
	const categories = store.getState().global.categories;

	return {
		type: 'GRAPH',
		types: [types.document.GET_DOCUMENT_REQUEST, types.document.GET_DOCUMENT_SUCCESS, types.document.GET_DOCUMENT_FAIL],
		data: {
			query: graphQueries.products.getProductById(),
			variables: { productId }
		},
		onSuccess: [
			data => getPlansByProduct(data.productsFiltersV2.data[0]._id),
			data => {
				AnalyticsHelper.eventOpenSingleScreen(data.productsFiltersV2.data[0], categories);
			}
		]
	};
};

export const getPlansByProduct = productId => ({
		type: 'GRAPH',
		types: [types.document.GET_PLANS_BY_PRODUCT_REQUEST, types.document.GET_PLANS_BY_PRODUCT_SUCCESS, types.document.GET_PLANS_BY_PRODUCT_FAIL],
		data: {
			query: graphQueries.products.getPlansByProduct(),
			variables: { productId }
		}
});

