import types from '../types';
import graphQueries from '../graphQueries';
import FilterHelper from '../../helpers/FilterHelper';
import {updateSidebarFilterCount} from './sidebarFilter';
import {createPoll} from './polls';
import { clearSidebarCalendarFilter, updateSidebarFilterFields } from './sidebarFilter';
import AnalyticsHelper from '../../helpers/AnalyticsHelper';
import store from '../index';

import {
  updateSidebarControlItem,
  clearSidebarControlFilter,
  clearSidebarFilter
} from './sidebarFilter';
import moment from "moment-timezone";

export const loadMoreProducts = (filters, isFavourite) => {
  const filter = isFavourite === void 0 ? {...filters} : {...filters, is_favourite:isFavourite }
	return {
		type: 'GRAPH',
		types: [types.products.LOAD_MORE_PRODUCTS_REQUEST, types.products.LOAD_MORE_PRODUCTS_SUCCESS, types.products.LOAD_MORE_PRODUCTS_FAIL],
		data: {
			query: graphQueries.products.getProducts(),
			variables: {
				filters : {...filter, is_active: true},
			}
		}
	};
};

export const getLastProduct = (filters) => {

  return {
    type: 'GRAPH',
    types: [types.products.GET_LAST_PRODUCTS_REQUEST, types.products.GET_LAST_PRODUCTS_SUCCESS, types.products.GET_LAST_PRODUCTS_FAIL],
    data: {
      query: graphQueries.products.getProducts(),
      variables: {
        filters: { is_ads: false, is_active: true, ...filters },
      }
    }
  };
};

export const fetchProductFilterDates = sectionId => ({
  type: 'GRAPH',
  types: [types.products.FETCH_PRODUCT_FILTER_DATES_REQUEST, types.products.FETCH_PRODUCT_FILTER_DATES_SUCCESS, types.products.FETCH_PRODUCT_FILTER_DATES_FAIL],
  data: {
    query: graphQueries.sidebarFilter.getDates(),
    variables: { sectionId }
  }
});

export const productCountBySectionToDates = (section, month, year) => ({
  type: 'GRAPH',
  types: [types.products.GET_PRODUCT_COUNT_REQUEST, types.products.GET_PRODUCT_COUNT_SUCCESS, types.products.GET_PRODUCT_COUNT_FAIL],
  data: {
    query: graphQueries.products.productCountBySectionToDates(),
    variables: {section, month, year}
  }
});

export const fetchCountiesTowns = sectionId => ({
  type: 'GRAPH',
  types: [types.products.FETCH_PRODUCT_FILTER_COUNTRIES_TOWNS_REQUEST, types.products.FETCH_PRODUCT_FILTER_COUNTRIES_TOWNS_SUCCESS, types.products.FETCH_PRODUCT_FILTER_COUNTRIES_TOWNS_FAIL],
  data: {
    query: graphQueries.sidebarFilter.getCountriesTowns(),
    variables: { sectionId }
  }
});
export const fetchPrices = sectionId => ({
  type: 'GRAPH',
  types: [types.products.FETCH_PRODUCT_PRICES_REQUEST, types.products.FETCH_PRODUCT_PRICES_SUCCESS, types.products.FETCH_PRODUCT_PRICES_FAIL],
  data: {
    query: graphQueries.sidebarFilter.getPrices(),
    variables: { sectionId }
  }
});

export const clearProducts = () => ({
  type: types.products.CLEAR_PRODUCTS
});

export const refreshProducts = (paginationOff, isFavourite) => {
  const formattedFilter = FilterHelper.formatFilterForRequest(store.getState().sidebarFilter);
  return {
    type: 'QUEUE',
    actions: [
      () => clearProducts(),
      state =>
        paginationOff
          ? loadMoreProducts({...formattedFilter.filter}, isFavourite)
          : loadMoreProducts({
            ...formattedFilter.filter,
            pagination: {
              limit: state.products.pagination.perPage,
              offset: state.products.pagination.page * state.products.pagination.perPage
            },
            ...formattedFilter.q
          }, isFavourite),
      () => updateSidebarFilterCount(formattedFilter.filterCount)
    ]
  }
};


export const refreshCalendarProducts = (paginationOff) => {
  const formattedFilter = FilterHelper.formatFilterForRequest(store.getState().sidebarFilter);
  const selectedDay = store.getState().calendar.selectedDay || void 0

  const calendarFormattedFilter = {
    ...formattedFilter.filter,
    dates_periods:  [{
      date_start: moment(selectedDay).startOf('day').format('YYYY-MM-DD HH:mm'),
      date_end: moment(selectedDay).endOf("day").format('YYYY-MM-DD HH:mm')
    }]
  }

  return {
    type: 'QUEUE',
    actions: [
      () => clearProducts(),
      state => loadMoreProducts({...calendarFormattedFilter}),
      () => updateSidebarFilterCount(formattedFilter.filterCount)
    ]
  }
};

export const updateProductSidebarFilter = (control, paginationOff) => ({
  type: 'QUEUE',
  actions: [
    () => control.clear ? clearSidebarControlFilter(control.controlName) : updateSidebarControlItem(control),
    () => refreshProducts(paginationOff)
  ]
});

export const updateCalendarProductSidebarFilter = (control) => ({
  type: 'QUEUE',
  actions: [
    () => control.clear ? clearSidebarControlFilter(control.controlName) : updateSidebarControlItem(control),
    () => refreshCalendarProducts()
  ]
});

export const clearProductSidebarFilter = (paginationOff) => ({
  type: 'QUEUE',
  actions: [
    () => clearSidebarFilter(),
    () => refreshProducts(paginationOff)
  ]
});

export const addProductToFavourite = favourite => {
  const products = store.getState().products.products || [];
  const categories = store.getState().global.categories || [];
  const product = products.find(item => item._id === favourite.product);
  AnalyticsHelper.eventAddToFavorites(product, categories);
  return {
    type: 'GRAPH',
    types: [types.products.ADD_PRODUCT_TO_FAVOURITE_REQUEST, types.products.ADD_PRODUCT_TO_FAVOURITE_SUCCESS, types.products.ADD_PRODUCT_TO_FAVOURITE_FAIL],
    data: {
      query: graphQueries.products.createFavourite(),
      variables: { favourite }
    }
  };
}

export const removeProductFromFavourite = filter => ({
  type: 'GRAPH',
  types: [types.products.REMOVE_PRODUCT_FROM_FAVOURITE_REQUEST, types.products.REMOVE_PRODUCT_FROM_FAVOURITE_SUCCESS, types.products.REMOVE_PRODUCT_FROM_FAVOURITE_FAIL],
  data: {
    query: graphQueries.products.deleteFavourite(),
    variables: { filter }
  }
});

/*
* Create product form, only for form with votes option
* */
export const createProduct = (product, poll) => ({
  type: 'GRAPH',
  types: [types.products.CREATE_PRODUCT_REQUEST, types.products.CREATE_PRODUCT_SUCCESS, types.products.CREATE_PRODUCT_FAIL],
  data: {
    query: graphQueries.products.createProduct(),
    variables: {product}
  },
  onSuccess: (data) => {
    return createPoll({
      ...poll,
      attached_entity: {
        target_type: "product",
        target_id: data.ProductCreateOne.recordId
      },
    })
  }

});

export const editProduct = product => ({
  type: 'GRAPH',
  types: [types.products.EDIT_PRODUCT_REQUEST, types.products.EDIT_PRODUCT_SUCCESS, types.products.EDIT_PRODUCT_FAIL],
  data: {
    query: graphQueries.products.editProduct(),
    variables: {product}
  }
});

