import types from '../types';
import graphQueries from '../graphQueries';

export const fetchScopes = () => ({
	type: 'GRAPH',
  types: [ types.global.FETCH_SCOPES_REQUEST, types.global.FETCH_SCOPES_SUCCESS, types.global.FETCH_SCOPES_FAIL ],
	data: {
		query: graphQueries.global.getScopes()
	}
});

export const fetchSections = () => ({
	type: 'GRAPH',
  types: [ types.global.FETCH_SECTIONS_REQUEST, types.global.FETCH_SECTIONS_SUCCESS, types.global.FETCH_SECTIONS_FAIL ],
	data: {
		query: graphQueries.global.getSections()
	}
});

export const fetchCategories = () => ({
	type: 'GRAPH',
  types: [ types.global.FETCH_CATEGORIES_REQUEST, types.global.FETCH_CATEGORIES_SUCCESS, types.global.FETCH_CATEGORIES_FAIL ],
	data: {
		query: graphQueries.global.getCategories()
	}
});

export const fetchPlans = () => ({
	type: 'GRAPH',
  types: [ types.global.FETCH_PLANS_REQUEST, types.global.FETCH_PLANS_SUCCESS, types.global.FETCH_PLANS_FAIL ],
	data: {
		query: graphQueries.global.getPlans()
	}
});

export const fetchCountries = () => ({
	type: 'GRAPH',
  types: [ types.global.FETCH_COUNTRIES_REQUEST, types.global.FETCH_COUNTRIES_SUCCESS, types.global.FETCH_COUNTRIES_FAIL ],
	data: {
		query: graphQueries.global.getCountries()
	}
});
export const fetchCurrencies = () => ({
	type: 'GRAPH',
  types: [types.global.FETCH_CURRENCIES_REQUEST, types.global.FETCH_CURRENCIES_SUCCESS, types.global.FETCH_CURRENCIES_FAIL],
	data: {
		query: graphQueries.global.getCurrencies()
	}
});
export const fetchSystemConfiguration = () => ({
	type: 'GRAPH',
  types: [types.global.FETCH_SYSTEM_CONFIGURATION_REQUEST, types.global.FETCH_SYSTEM_CONFIGURATION_SUCCESS, types.global.FETCH_SYSTEM_CONFIGURATION_FAIL],
	data: {
		query: graphQueries.global.getSystemConfiguration()
	}
});

export const fetchAds = () =>  ({
  type: 'GRAPH',
  types: [types.global.FETCH_ADS_REQUEST, types.global.FETCH_ADS_SUCCESS, types.global.FETCH_ADS_FAIL],
  data: {
    query: graphQueries.products.getProducts(),
    variables: {
      filters: {
        is_ads: true,
        sort: "date_publish"
      }
    }
  }
});


/*  Global Modal Actions */

export const planModalShow = () => ({
	type: types.global.PLANS_MODAL_SHOW
});
export const planModalHide = () => ({
	type: types.global.PLANS_MODAL_HIDE
});
