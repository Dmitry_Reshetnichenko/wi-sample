import types from "../types";
import graphQueries from "../graphQueries";
import { achievementModalHide } from './modals'

export const fetchAchievements = user_id => {
  return {
    type: 'GRAPH',
    types: [types.achievements.FETCH_ACHIEVEMENTS_REQUEST, types.achievements.FETCH_ACHIEVEMENTS_SUCCESS, types.achievements.FETCH_ACHIEVEMENTS_FAIL],
    data: {
      query: graphQueries.achievements.fetchAchievements(),
      variables: { user_id }
    }
  }
};

export const activateAchievement = ({ user_id, achievement_id }) => {
  return {
    type: 'GRAPH',
    types: [types.achievements.ACTIVATE_ACHIEVEMENT_REQUEST, types.achievements.ACTIVATE_ACHIEVEMENT_SUCCESS, types.achievements.ACTIVATE_ACHIEVEMENT_FAIL],
    data: {
      query: graphQueries.achievements.activateAchievement(),
      variables: { user_id, achievement_id }
    },
    onSuccess: [
      () => fetchAchievements(user_id),
      achievementModalHide
    ]
  }
};

export const deactivateAchievement = ({ user_id, achievement_id }) => {
  return {
    type: 'GRAPH',
    types: [types.achievements.DEACTIVATE_ACHIEVEMENT_REQUEST, types.achievements.DEACTIVATE_ACHIEVEMENT_SUCCESS, types.achievements.DEACTIVATE_ACHIEVEMENT_FAIL],
    data: {
      query: graphQueries.achievements.deactivateAchievement(),
      variables: { user_id, achievement_id }
    },
    onSuccess: [
      () => fetchAchievements(user_id),
      achievementModalHide
    ]
  }
};

export const emitAchievementEvent = ({ user_id, type, product_id }) => {
  return {
    type: 'GRAPH',
    types: [types.achievements.EMIT_ACHIEVEMENT_EVENT_REQUEST, types.achievements.EMIT_ACHIEVEMENT_EVENT_SUCCESS, types.achievements.EMIT_ACHIEVEMENT_EVENT_FAIL],
    data: {
      query: graphQueries.achievements.emitAchievementEvent(),
      variables: { userId: user_id, type, productId: product_id }
    }
  }
};

