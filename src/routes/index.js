import React from 'react';
import Journal from '../pages/Journal';
import Root from '../pages/Root';
import Communication from '../pages/Communication';
import Community from '../pages/Communication/Community';
import GlobalSuccess from '../pages/Global/GlobalSuccess';
import NewPostPage from '../pages/Communication/NewPostPage';
import EditPostPage from '../pages/Communication/EditPostPage';
import Discussion from '../pages/Communication/Discussion';
import Calendar from '../pages/Calendar';
import Shop from '../pages/Shop';
import Education from '../pages/Education';
import Notification from '../pages/Notifications/Notifications';
import Achievements from '../pages/Education/Achievements';
import ChakrasTasksPage from '../pages/Education/ChakrasTasksPage';
import Chakras from '../pages/Education/Chakras';
import Questions from '../pages/Education/Questions';
import Statistics from '../pages/Education/Statistics';
import ChakrasResultsPage from '../pages/Education/ChakrasResultsPage';
import ChakrasKnowlegePage from '../pages/Education/ChakrasKnowlegePage';
import Articles from '../pages/Journal/Articles';
import Videos from '../pages/Journal/Videos';
import Premium from '../pages/Journal/Premium';
import Consultation from '../pages/Journal/Consultations';
import Prizes from '../pages/Education/Prizes';

import Document from '../pages/Document';
import AllEvents from "../pages/calendar/AllEvents";
import UserEvents from "../pages/calendar/UserEvents";

import Profile from '../pages/Profile';
import Info from '../pages/Profile/Info';
import ActivePlans from '../pages/Profile/ActivePlans';
import Payments from '../pages/Profile/Payments';
import Support from '../pages/Profile/Support';
import Invites from '../pages/Profile/Invites';

import Plan from '../pages/Plan';

import Favourites from '../pages/Favourites';
import GlobalSearchResultsPage from '../pages/GlobalSearchResultsPage';
import ForgotPassword from '../pages/ForgotPassword';

import SiteMap from '../pages/SiteMap';

import Impersonate from '../pages/Impersonate';
import Maintenance from '../pages/Maintenance';

const routes = [
  {
    path: '/',
    component: Root,
    exact: true
  },
  {
    path: '/access/:token',
    component: Impersonate,
    exact: true
  },
  {
    path: '/global_success',
    component: GlobalSuccess,
    name: 'global_success'
  },
  {
    path: '/journal',
    component: Journal,
    name: 'magazine',
    routes: [
      {
        path: '/journal/:section/:id',
        component: Document,
        name: 'document'
      },
      {
        path: '/journal/:section/:collectionId/:id',
        component: Document,
        name: 'documentInCollection'
      },
      {
        path: '/journal/articles',
        component: Articles,
        name: 'articles',
      },
      {
        path: '/journal/video',
        component: Videos,
        name: 'video'
      },
      {
        path: '/journal/premium',
        component: Premium,
        name: 'premium'
      },
      {
        path: '/journal/consultation',
        component: Consultation,
        name: 'consultation'
      }
    ]
  },

  {
    path: '/communication',
    component: Communication,
    name: 'communication',
    routes: [
      {
        path: '/communication/community/edit/:id',
        component: EditPostPage,
        name: 'edit'
      },

      {
        path: '/communication/community/create',
        component: NewPostPage,
        name: 'create'
      },
      {
        path: '/communication/:section/:id',
        component: Document,
        name: 'document'
      },
      {
        path: '/communication/discussion',
        component: Discussion,
        name: 'discussion'
      },
      {
        path: '/communication/community',
        component: Community,
        name: 'community'
      },
      {
        path: '/communication/new_post',
        component: NewPostPage,
        name: 'new_post'
      }
    ]
  },
  {
    path: '/calendar',
    component: Calendar,
    name: 'calendar',
    routes: [
      {
        path: '/calendar/:section/:id',
        component: Document,
        name: 'document'
      },
      {
        path: '/calendar/all_events',
        component: AllEvents,
        name: 'all_events'
      },
      {
        path: '/calendar/user_events',
        component: UserEvents,
        name: 'user_events',
      }
    ]
  },
  {
    path: '/shop',
    component: Shop,
    name: 'store',
  },
  {
    path: '/education',
    component: Education,
    name: 'progress',
    routes: [
      {
        path: '/education/chakras',
        component: Chakras
      },
      {
        path: '/education/chakras/questions',
        component: Questions
      },
      {
        path: '/education/journal/:section/:id',
        component: Document,
        name: 'document'
      },
      {
        path: '/education/journal/prizes',
        component: Prizes,
        name: 'prizes'
      },
      {
        path: '/education/chakras/statistics',
        component: Statistics
      },
      {
        path: '/education/chakras/results',
        component: ChakrasResultsPage
      },
      {
        path: '/education/chakras/knowledge',
        component: ChakrasKnowlegePage,
      },
      {
        path: '/education/chakras/tasks',
        component: ChakrasTasksPage
      },
      {
        path: '/education/achievements',
        component: Achievements
      }
    ]
  },
  {
    path: '/notifications',
    component: Notification,
    name: 'notifications'
  },
  {
    path: '/profile',
    component: Profile,
    name: 'profile',
    routes: [
      {
        path: '/profile/info',
        component: Info
      },
      {
        path: '/profile/plans',
        component: ActivePlans
      },
      {
        path: '/profile/payments',
        component: Payments
      },
      {
        path: '/profile/support',
        component: Support
      },
      {
        path: '/profile/invites',
        component: Invites
      },
    ]
  },
  {
    path: '/favourites',
    component: Favourites,
    name: 'favourites'
  },
  {
    path: '/global_search_results',
    component: GlobalSearchResultsPage,
  },
  {
    path: '/forgot-password/:resetToken',
    component: ForgotPassword,
    name: 'forgotPassword'
  },
  {
    path: '/plan/:id',
    component: Plan,
    name: 'single-plan'
  },
  {
    path: '/maintenance',
    component: Maintenance,
    name: 'maintenance'
  },
  {
    path: '/sitemap',
    component: SiteMap,
  },
  {
    path: '*',
    component: () => <h1>404</h1>,
    name: 'not-found'
  },
];

export default routes;
