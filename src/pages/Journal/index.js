import React from 'react';
import JournalMenu from '../../components/Menu/index';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import routes from '../../routes';
import { Container, Menu } from '../../components/layouts/Main';

class Journal extends React.Component {

  menuItems = [
    {
      label: 'Статьи',
      path: '/journal/articles',
    },
    {
      label: 'Видео',
      path: '/',
      exact: true,
      isActive: (match, location) => match || location.pathname.match(/journal\/video/)
    },
    {
      label: 'Курсы',
      path: '/journal/premium'
    },
    {
      label: 'Консультации',
      path: '/journal/consultation'
    },
  ];

  getSectionIdByName = name => {
    const section = this.props.sections
      .filter(section => section.parents[0] === this.props.scopeId)
      .find(section => section.name === name);
    return section ? section._id : null;
  };

  render() {
    const journalRoutes = routes.find(route => route.name === 'magazine').routes;
    return (
      <Container>
        <Route exact path="/journal" render={() => (<Redirect to="/"/>)}/>
        <Menu>
          <JournalMenu menuItems={this.menuItems} />
        </Menu>
          {journalRoutes.map((route, index) => (
            <Route
              key={index}
              path={route.path}
              exact={true}
              render={props =>
                <route.component
                  sectionId={this.getSectionIdByName(route.name)}
                  {...props}
                />
              }
            />
            ))}
      </Container>
    );
  }
}

export default connect(
  state => ({
    sections: state.global.sections,
  })
)(Journal);
