import React from 'react';
import { connect } from 'react-redux';
import moment from 'moment-timezone';

import { showOnBoardModal } from '../../store/actions/onboard'
import OnBoardTypes from '../../constants/OnBoardTypes';

import ProductPage from '../../components/journal/ProductPage';
import FavoriteIcon from '../../images/icons/favorite.svg';

import FilterTypes from '../../constants/FilterTypes';
import FilterControlTypes from '../../constants/FilterControlTypes';
import ProductTypes from "../../constants/ProductTypes";

class Articles extends React.Component {
  state = {
  	sectionDefined: false,
		sidebarConfig: {
			nav: [
				{
					label: 'Избранное',
					icon: FavoriteIcon,
					href: '/favourites'
				}
			],
			filter: {
				title: 'фильтры',
				controls: [
					{
						controlType: FilterControlTypes.CHECKBOX_GROUP,
						title: 'тип:',
						type: FilterTypes.PRODUCT_TYPES,
						name: 'productTypes',
						allCheckbox: true,
						items: [
							{ label: 'Аудио', value: ProductTypes.AUDIO },
						]
					},
					{
						controlType: FilterControlTypes.CHECKBOX_GROUP,
						title: 'рубрика:',
						type: FilterTypes.CATEGORIES,
						name: 'productCategories',
						allCheckbox: true,
						items: []
					},
					{
						controlType: FilterControlTypes.CHECKBOX_GROUP,
						title: 'период:',
						type: FilterTypes.DATES,
						name: 'productDates',
						allCheckbox: true,
						scrollable: true,
						items: []
					}
				]
			}
		}
  };

  componentDidMount() {
  	const { user, showOnBoardModal } = this.props;
 
		showOnBoardModal(
			user && user.prevPayment
								? OnBoardTypes.JOURNAL_ARTICLES_OLD
								: OnBoardTypes.JOURNAL_ARTICLES_NEW
		);
	}
	
	sidebarConfig = () => {
  	let config = { ...this.state.sidebarConfig };

  	config.filter.controls.find(control => control.type === FilterTypes.CATEGORIES).items =
			this.props.categories
				.filter(category => category.parents[0] === this.props.sectionId)
				.sort((a, b) => moment(a.createdAt).diff(b.createdAt))
				.map(category => ({ label: category.title, value: category._id }));

  	config.filter.controls.find(control => control.type === FilterTypes.DATES).items =
			this.props.filterDates.map(date => ({ label: moment(date).format('MMMM YYYY'), value: date }));

  	return config;
	};

  render() {
    return (
    	<React.Fragment>
    		<ProductPage sectionId={this.props.sectionId} sidebarConfig={this.sidebarConfig()} filterFields={{ is_ads: null }}/>
			</React.Fragment>
    );
  }
}

export default connect(
	state => ({
		user: state.auth.user,
		categories: state.global.categories,
		filterDates: state.products.filterDates,
	}),
	dispatch => ({
		showOnBoardModal: data => dispatch(showOnBoardModal(data))
	})
)(Articles);

