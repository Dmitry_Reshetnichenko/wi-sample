import React, {useEffect} from 'react';
import {connect} from "react-redux";
import {compose} from 'redux'
import moment from 'moment-timezone';
import {Container, Menu} from "../../components/layouts/Main";
import MenuItem from "../../components/Menu/MenuItem";
import NotificationDateItem from "../../components/partials/Notification/NotificationDateItem";
import NotificationsActionsBar from "../../components/partials/Notification/NotificationsActionsBar";
import NotificationList from "../../components/partials/Notification/NotificationList";
import {getNotifications} from '../../store/actions/notifications'
import Loader from "../../components/partials/Loader";
import {withRoutAuthCompose} from "../../components/HOC/withRout";

const Notifications = ({user, getNotifications, notifications, notificationsLoading}) => {

  useEffect(() => {
    user && !notificationsLoading && getNotifications({filter: {user: user._id}});
  }, [user]);

  const todayNotifications = notifications.filter(i => moment(i.createdAt).isSame(moment(), 'day'));
  const previousNotifications = notifications.filter(i => !moment(i.createdAt).isSame(moment(), 'day'));

  return (
    <Container>
      <Menu>
        <div className="journal-menu">
          <div className="journal-menu__nav">
            <div className="journal-menu__nav">
              <MenuItem path={'/notifications'} label={'Уведомления'}/>
            </div>
          </div>
        </div>
      </Menu>
      <Loader visible={Boolean(notificationsLoading)}/>
      <div className="notifications">
        <NotificationsActionsBar/>
        <NotificationDateItem today isEmptyState={!Boolean(todayNotifications.length)}/>
        <NotificationList notifications={todayNotifications} today/>
        <NotificationDateItem isEmptyState={!Boolean(previousNotifications.length)}/>
        <NotificationList notifications={previousNotifications}/>
      </div>
    </Container>
  );
};

export default compose(
  withRoutAuthCompose(''),
  connect(
  state => ({
    user: state.auth.user,
    notifications: state.notifications.notifications,
    notificationsLoading: state.notifications.loading
  }),
  dispatch => ({
    getNotifications: (filter) => dispatch(getNotifications(filter))
  })
))(Notifications);

