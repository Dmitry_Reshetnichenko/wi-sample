import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {Container, Page, Content, Menu} from '../components/layouts/Main';
import GlobalNav from '../components/partials/GlobalNav';
import Search from "../components/partials/Search";
import WidgetList from '../components/journal/WidgetList';
import {bindActionCreators} from "redux";
import FilterHelper from "../helpers/FilterHelper";
import {
  loadMoreProducts,
  refreshProducts,
  clearProducts,
} from '../store/actions/products';
import {
  updateSidebarFilterFields,
  clearSidebarFilter,
  updateSidebarFilterCount
} from '../store/actions/sidebarFilter';
import {addSearchValue} from "../store/actions/sidebarFilter";

const GlobalSearchResultsPage = (
  {
    products,
    hasMore,
    loading,
    pagination,
    updateSidebarFilterFields,
    clearSidebarFilter,
    loadMoreProducts,
    refreshProducts,
    sidebarFilter,
    clearProducts,
  }) => {

  const searchValue = sidebarFilter.filter.searchValue && sidebarFilter.filter.searchValue.value;

  useEffect(() => {
    updateSidebarFilterFields({section: null});
    clearSidebarFilter();
    clearProducts();
  }, []);

  useEffect(() => {
    sidebarFilter.filter.searchValue && refreshProducts();
  }, [sidebarFilter.filter.searchValue]);

  const loadMore = () => {

    let formattedFilter = FilterHelper.formatFilterForRequest(sidebarFilter);
    const filter = {
      ...formattedFilter.filter,
      section: void 0,
      pagination: {
        limit: pagination.perPage,
        offset: pagination.page * pagination.perPage
      },
      q: searchValue || ''
    }

    loadMoreProducts(filter)
  };

  return (
    <Container nosidebar>
      <div className="search-results">
        <div className="search-results__search-row">
          <div className="search-results__left-container">
            <GlobalNav
              label={'Поиск'}
              isGlobalSearchPage
            />
          </div>
          <div className="search-results__right-container">
            <Search isGlobalSearchPage />
          </div>
        </div>
      </div>
      <Page>
        <Content>
          <WidgetList
            products={products}
            loadMoreProducts={() => loadMore()}
            pagination={pagination}
            loading={loading}
            hasMore={hasMore}
            search
          />
        </Content>
      </Page>
    </Container>
  );
};

const mapStateToProps = ({state}) => ({
  sections: state.global.sections,
  products: state.products.products,
  loading: state.products.loading,
  pagination: state.products.pagination,
  hasMore: state.products.hasMore,
  sidebarFilter: state.sidebarFilter
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({
    loadMoreProducts,
    updateSidebarFilterFields,
    refreshProducts,
    updateSidebarFilterCount,
    addSearchValue,
    clearSidebarFilter,
    clearProducts
  }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(GlobalSearchResultsPage);

