import React, { useEffect } from 'react';

import { connect } from 'react-redux';
import { fetchDocument } from '../store/actions/document';
import { Page, Sidebar, Content } from '../components/layouts/Main';
import DocumentContent from '../components/DocumentContent';
import SidebarProducts from '../components/partials/Sidebars/SidebarProducts';
import SectionNames from "../constants/SectionNames";
import Loader from '../components/partials/Loader';

const Document  = ({fetchDocument, match, sections, polls, document}) => {

  const sectionId = sections.find(section => section.name === match.params.section)._id;
  const isEventsSection = sections.find(item => item._id === sectionId).name === SectionNames.ALL_EVENTS || SectionNames.USER_EVENTS
  const currPoll = polls.find(poll => (poll.attached_entity && poll.attached_entity.target_id) === (document._id));

  useEffect(() => {fetchDocument(match.params.id)}, [match.params.id]);

  return (
    <React.Fragment>
      <Page>
        <Sidebar>
          <SidebarProducts
            sectionId={sectionId}
            currentId={document && document._id}
            calendarSection={isEventsSection}
          />
        </Sidebar>
        <Content>
          <Loader visible={!Boolean(document && document._id)}/>
          {document && document._id &&
          <DocumentContent
            currPoll={currPoll}
            calendarSection={isEventsSection}
            sectionId={sectionId}
            document={document}
            match={match}
          />}
        </Content>
      </Page>
    </React.Fragment>
  );
}

export default connect(
  state => ({
    document: state.document.document,
    sections: state.global.sections,
    polls: state.polls.polls,
  }),
  dispatch => ({
    fetchDocument: id => dispatch(fetchDocument(id))
  })
)(Document);