import ProductTypes from '../constants/ProductTypes';
import ScopeNames from '../constants/ScopeNames';
import SectionNames from '../constants/SectionNames';

class ProductHelper{
	static isCollection(product){
		return Boolean(~[ProductTypes.COLLECTION, ProductTypes.MARATHON].indexOf(product.ekind));
	}
	static hasImage(product){
		return product && product.image && product.image.thumb_web && product.image.thumb_web.url &&  product.image && product.image.full.url;
	}
	static isCalendar(){
		return location.pathname.split('/')[1] === ScopeNames.CALENDAR;
	}
	static isConsultations(){
		return location.pathname.split('/')[2] === SectionNames.CONSULTATION;
	}
	static isJournal(){
		return location.pathname.split('/')[1] === ScopeNames.JOURNAL;
	}
	static isCommunication(){
		return location.pathname.split('/')[1] === ScopeNames.COMMUNICATION;
	}

	static isCommunity(){
		return location.pathname.split('/')[2] === ScopeNames.COMMUNITY;
	}

	static isDiscussion(){
		return location.pathname.split('/')[2] === ScopeNames.DISCUSSION;
	}
	static isPremium(){
		return location.pathname.split('/')[2] === ScopeNames.PREMIUM;
	}

	static getSection(){
		return location.pathname === '/' ? 'video' : location.pathname.split('/')[2];
	}
}

export default ProductHelper;
