class AdsHelper {
	static rerunAdsScript(){
		if(window.reviveAsync && reviveAsync[process.env.AD_REVIVE_ID])
			reviveAsync[process.env.AD_REVIVE_ID].apply(reviveAsync[process.env.AD_REVIVE_ID].detect());
	}
}

export default AdsHelper;