import FilterTypes from '../constants/FilterTypes';
import FilterControlTypes from '../constants/FilterControlTypes';

import moment from 'moment-timezone';

class FilterHelper {
  static formatFilterForRequest(sidebarFilter) {

    let filter = {};
    let filterCount = 0;

    for (let control of Object.values(sidebarFilter.filter)) {

      switch (control.controlType) {

        case FilterControlTypes.CHECKBOX_GROUP:
          let filterData;
          if (control.alwaysFilter) {
            let allChecked = control.items.every(item => !item.checked);
            filterData = allChecked
              ? control.items.reduce((acc, item) => [...acc, item.value], [])
              : control.items.reduce((acc, item) => item.checked ? [...acc, item.value] : acc, []);
          } else
            filterData = control.items.reduce((acc, item) => item.checked ? [...acc, item.value] : acc, []);

          filterCount += filterData.length;

          if (filterData.length)
            switch (control.type) {
              case FilterTypes.CATEGORIES:
                filter.topics_ids = filterData;
                break;
              case FilterTypes.PRODUCT_TYPES:
                filter.ekinds = filterData;
                break;
              case FilterTypes.DATES:
                filter.dates_periods =
                  filterData.reduce((acc, date) =>
                      [
                        ...acc,
                        {
                          date_start: moment(date).format('YYYY-MM-DD HH:mm:ss'),
                          date_end: moment(date).add(1, 'months').format('YYYY-MM-DD HH:mm:ss')
                        }
                      ]
                    , []);
                break;
              case FilterTypes.COUNTRIES:
                filter.country_id = filterData;
                break;
              case FilterTypes.CITIES:
                filter.city_id = filterData;
                break;
              case FilterTypes.CALENDAR:
                filter.dates_periods =
                  filterData.reduce((acc, date) =>
                      [
                        ...acc,
                        {
                          date_start: moment(date).startOf('day').format('YYYY-MM-DD HH:mm'),
                          date_end: moment(date).endOf("day").format('YYYY-MM-DD HH:mm')
                        }
                      ]
                    , []);
                break;
            }
          break;

        case  FilterControlTypes.RANGE_SLIDER:
          switch (control.type) {
            case FilterTypes.PRICE_RANGE:
              if (control.selectedRange && control.selectedRange[0] && control.selectedRange[1])
                filter.price = {
                  from: control.selectedRange[0],
                  to: control.selectedRange[1]
                };
              break;
          }
          break;
      }
    }

    for (let key in sidebarFilter.fields)
      if (sidebarFilter.fields[key] === null)
        delete sidebarFilter.fields[key];
    return {
      filter: {
        ...filter,
        ...sidebarFilter.fields
      },
      filterCount,
      section:void 0,
      q: {
        q: sidebarFilter.filter.searchValue && sidebarFilter.filter.searchValue.value
      }
    };
  }
}

export default FilterHelper;