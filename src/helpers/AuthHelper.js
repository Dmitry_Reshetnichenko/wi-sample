const TOKEN_KEY = 'WI_TOKEN';
const TYPE_AUTH_KEY = 'WI_TYPE_AUTH';

class AuthHelper {
	static setToken(token){
			localStorage.setItem(TOKEN_KEY, token);
	}
	static removeToken(){
			localStorage.removeItem(TOKEN_KEY);
			localStorage.removeItem(TYPE_AUTH_KEY);
	}
	static tokeIsDefined(){
		return Boolean(localStorage.getItem(TOKEN_KEY));
	}
	static getToken(){
		return localStorage.getItem(TOKEN_KEY);
	}
	static setTypeAuth(type){
		localStorage.setItem(TYPE_AUTH_KEY, type);
	}
	static getTypeAuth(){
		return localStorage.getItem(TYPE_AUTH_KEY);
	}
	static removeTypeAuth(){
		localStorage.removeItem(TYPE_AUTH_KEY);
	}
}

export default AuthHelper;