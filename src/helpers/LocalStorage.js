const LOCAL_STORAGE_PREFIX = 'wi';

const storageKeyFormat = key => `${LOCAL_STORAGE_PREFIX}_${key}`;

class LocalStorage {

  static setValue(key, value, json = false) {
    return localStorage[storageKeyFormat(key)] = (json ? JSON.stringify(value) : value);
  }

  static getValue(key, json = false) {
    return json ? JSON.parse(localStorage[storageKeyFormat(key)]) : localStorage[storageKeyFormat(key)];
  }

  static valueExists(key) {
    return localStorage[storageKeyFormat(key)] !== undefined;
  }
  

}

export default LocalStorage;