const regexp = /([^\"=]{2}|^)((https?|ftp):\/\/\S+[^\s.,> )\];'\"!?])/;
const subst = '$1<a style="color: #3D81DB ; text-decoration: underline" href="$2" target="_blank">$2</a>';

const addLinkInText = text => {
  return  text.replace(regexp, subst)
};

export default addLinkInText
