const PublishStatusNames = {
  PUBLISHED: 'Published',
  NOT_PUBLISHED: 'NotPublished',
  ON_MODERATION: 'OnModeration',
  DENIED: 'Denied',
};

export  default PublishStatusNames