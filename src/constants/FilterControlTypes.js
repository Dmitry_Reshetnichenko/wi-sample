const FilterControlTypes = {
	CHECKBOX_GROUP: 'checkboxGroup',
	RADIO_GROUP: 'radioGroup',
	CALENDAR: 'calendar',
	RANGE_SLIDER: 'rangeSlider',
};

export default FilterControlTypes;