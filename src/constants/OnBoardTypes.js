const OnBoardTypes = {
	JOURNAL_ARTICLES_NEW: 'journal_articles_new',
	JOURNAL_ARTICLES_OLD: 'journal_articles_old',
	JOURNAL_PREMIUM: 'journal_premium',
	ARTICLE: 'article',
	CALENDAR: 'calendar',
	COMMUNITY_DISCUSSION: 'community_discussion',
	PROFILE_INFO: 'profile_info',
	PROFILE_PLANS: 'profile_plans'
};

export default OnBoardTypes;