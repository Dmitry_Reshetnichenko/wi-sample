const ScopesNames = {
	STORE: 'store',
	COMMUNICATION: 'communication',
	COMMUNITY: 'community',
	DISCUSSION: 'discussion',
	CALENDAR: 'calendar',
	PROGRESS: 'progress',
	JOURNAL: 'journal',
	PREMIUM: 'premium'
};

export default ScopesNames;