const SectionNames = {
	ARTICLES: 'articles',
	VIDEOS: 'video',
	PREMIUM: 'premium',
	PRIZES: 'prizes',
	ALL_EVENTS: 'all_events',
	USER_EVENTS: 'user_events',
	DISCUSSION: 'discussion',
	ACHIEVEMENTS: 'achievements',
	CHAKRAS: 'chakras',
	COMMUNITY: 'community',
	CONSULTATION: 'consultation',
	EDUCATION: 'education',
	CALENDAR: 'calendar',
	COMMUNICATION: 'communication'
};

export default SectionNames;