
const ProductTypes = {
  ARTICLE: 'article',
  VIDEO: 'video',
  AUDIO: 'audio',
  MOON_CALENDAR: 'mooncalendar',
  WEBINAR: 'webinar',
  OFFLINE_EVENT: 'offlineevent',
  COLLECTION: 'collection',
  MARATHON: 'marathon'
};

export  default ProductTypes