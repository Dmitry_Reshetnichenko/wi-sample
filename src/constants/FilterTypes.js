const FilterTypes = {
	CATEGORIES: 'categories',
	DATES: 'dates',
	PRODUCT_TYPES: 'productTypes',
	CITIES: 'cities',
	COUNTRIES: 'countries',
  CALENDAR: 'calendar',
	OWN: 'own',
	PRICE_RANGE: 'priceRange',
  THEMES:'themes'
};

export default FilterTypes;