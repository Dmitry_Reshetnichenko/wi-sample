import React from 'react';
import { connect } from 'react-redux';
import { logout } from '../../../../store/actions/auth';

import InfoIcon from '../../../../images/icons/profile/profile-info.svg';
import PaymentIcon from '../../../../images/icons/profile/profile-payment.svg';
import InvitesIcon from '../../../../images/icons/profile/profile-invites.svg';
import PlanIcon from '../../../../images/icons/profile/profile-plan.svg';
import SupportIcon from '../../../../images/icons/profile/profile-support.svg';
import LogoutIcon from '../../../../images/icons/16px/logout.svg';
import MoreDetailsIcon from '../../../../images/icons/profile/more-details.svg';

import NavItem from './NavItem';

const navItems = [
	{
		path: '/profile/info',
		label: 'Личные данные',
		icon: InfoIcon
	},
	{
		path: '/profile/payments',
		label: 'Платежные данные',
		icon: PaymentIcon
	},
	{
		path: '/profile/plans',
		label: 'Планы подписок',
		icon: PlanIcon
	},
	{
		path: '/profile/invites',
		label: 'Приглашения',
		icon: InvitesIcon
	}
];

const menuItems = [
	{
		path: '/profile/support',
		label: 'Служба поддержки',
		icon: SupportIcon,
		description: 'Обратитесь в службу поддержки, если у Вас остались вопросы или Вы столкнулись  с какими-либо трудностями.'
	},
	{
		path: '/journal/articles/5e2f14cf6137d03aea4e7957',
		icon: MoreDetailsIcon,
		label: 'Условия Подписки'
	},
	{
		path: '/journal/articles/5e32a6378bf8eb541ed39889',
		icon: MoreDetailsIcon,
		label: 'Политику Конфиденциальности'
	}
];
const SidebarProfile = ({ logout }) => {
	return (
		<div className="sidebar-profile">
			<div className="sidebar-profile__nav">
				{navItems.map((item, index) => <NavItem item={item} key={index}/>)}
			</div>
			<div className="sidebar-profile__divider"/>
			{menuItems.map((item, index) => <NavItem item={item} key={index}/>)}
			<div className="sidebar-profile__divider"/>
			<NavItem onClick={logout} item={{ label: 'Выход', icon: LogoutIcon }}/>
		</div>
	);
};

export default connect(
	null,
	dispatch => ({
		logout: () => dispatch(logout())
	})
)(SidebarProfile);
