import React from 'react';
import SVG from "react-inlinesvg";
import {NavLink} from "react-router-dom";

const NavItem = ({ item, ...rest }) => {
	return (
		item.path
				? <NavLink {...rest} to={item.path} className="sidebar-profile__nav-item" activeClassName="sidebar-profile__nav-item_active">
						<div className="sidebar-profile__nav-item-content">
							<div className="sidebar-profile__nav-item-icon">
								<SVG src={item.icon}/>
							</div>
							{item.label}
						</div>
						{item.description && <div className="sidebar-profile__nav-item-description">{item.description}</div>}
					</NavLink>
				: <div {...rest} className="sidebar-profile__nav-item">
						<div className="sidebar-profile__nav-item-content">
							<div className="sidebar-profile__nav-item-icon">
								<SVG src={item.icon}/>
							</div>
							{item.label}
						</div>
						{item.description &&
							<div className="sidebar-profile__nav-item-description">
								{item.description}
							</div>}
					</div>

	);
};

export default NavItem;