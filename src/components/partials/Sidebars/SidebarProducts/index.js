import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {loadMoreProducts, refreshProducts} from '../../../../store/actions/sidebarProducts';
import {updateSidebarFilterFields} from '../../../../store/actions/sidebarFilter';
import FilterHelper from '../../../../helpers/FilterHelper';

import ListItem from './ListItem';
import moment from 'moment-timezone';
import Loader from "../../Loader";
import MonthDivider from './MonthDivider';
import {Link} from "react-router-dom";
import InfiniteScroll from "react-infinite-scroll-component";
import ProductHelper from '../../../../helpers/ProductHelper';

const SidebarProducts = (
  {
    refreshProducts,
    currentId,
    auth,
    products,
    sectionId,
    pagination,
    sidebarFilter,
    loadMoreProducts,
  }) => {

  useEffect(() => {
    const limit = (ProductHelper.isDiscussion() && !(auth.user && auth.user.goldUser)) && 1;
    refreshProducts(productsFilter(), limit)
  }, [auth.user]);

  const productsFilter = () => ({
    ...FilterHelper.formatFilterForRequest(sidebarFilter).filter,
    dates_periods: [],
    section: sectionId,
    pagination: {
      offset: pagination.page * pagination.perPage,
      limit: pagination.perPage
    }
  });

  const formatType = ProductHelper.isCalendar() ? 'YYYY-MM-DD' : 'YYYY-MM';
  const path = location.pathname.split('/').splice(0, 3).join('/');

  const productsDivided = () =>
  [...new Set(products.map(o => JSON.stringify(o)))].map(s => JSON.parse(s)).reduce((acc, article) => {
    let date = moment(article.date_publish).format(formatType);
    if (!acc[date])
      acc[date] = [];
    acc[date].push(article);
    return acc;
  }, {});

  return (
    <div id="sidebar-products-scroll" className="sidebar-products">
      <InfiniteScroll
        dataLength={products.length}
        hasMore
        next={() => loadMoreProducts(productsFilter())}
        scrollThreshold={0.6}
        scrollableTarget="sidebar-products-scroll"
        loader={
          <div className="sidebar-products__loader">
            <Loader bgNone visible/>
          </div>
        }
      >
        {Object.keys(productsDivided()).sort().reverse().map((date, index) =>
          <React.Fragment key={index}>
            <MonthDivider date={date} isPrice={ProductHelper.isCalendar()}/>
            {
              productsDivided()[date].map((product, index) => (
                <Link to={`${path}/${product._id}`} key={index}>
                  <ListItem active={product._id === currentId}
                            key={index}
                            product={product}
                            isPrice={ProductHelper.isCalendar()}/>
                </Link>
              ))
            }
          </React.Fragment>
        )}
      </InfiniteScroll>
    </div>
  )
};

export default connect(
  state => ({
    auth: state.auth,
    products: state.sidebarProducts.products,
    sidebarFilter: state.sidebarFilter,
    pagination: state.sidebarProducts.pagination,
    hasMore: state.sidebarProducts.hasMore
  }),
  dispatch => ({
    loadMoreProducts: filters => dispatch(loadMoreProducts(filters)),
    refreshProducts: (filters, limit) => dispatch(refreshProducts(filters, limit)),
    updateSidebarFilterFields: fields => dispatch(updateSidebarFilterFields(fields))
  })
)(SidebarProducts);