import React from 'react';
import moment from 'moment-timezone';
import classNames from 'classnames';

const ListItem = ({product, active, isPrice}) => {
  const date = () => {
    let date = moment(product.date_publish);
    return {
      day: date.format('DD'),
      month: date.format('MMM').replace(/\./g, '')
    };
  };

  const itemClasses = classNames('sidebar-products__item', {
    'sidebar-products__item_active': active,
    'sidebar-products__item_centerd': isPrice,
  });

  const item = classNames('sidebar-products__item-date-container', {
    'sidebar-products__item-price': isPrice,
  });

  const priceBlock = isPrice && (
    <div>
      {product.price.sale_price ? `$${product.price.sale_price}` : 'free'}
    </div>
  )

  const view = priceBlock || (
    <React.Fragment>
      <div>{date().day}</div>
      <div>{date().month}</div>
    </React.Fragment>
  )

  return (
    <div className={itemClasses}>
      <div className="sidebar-products__item-head">
        <div className="sidebar-products__item-date">
          <div className={item}>{view}</div>
        </div>
        <div className="sidebar-products__item-title">{product.title}</div>
      </div>
      <div className="sidebar-products__item-author">
        {product.author && product.author.name}
      </div>
    </div>
  )
};

export default ListItem;