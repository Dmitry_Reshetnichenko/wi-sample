import React from 'react';
import moment from 'moment-timezone';

const MonthDivider = ({ date, isPrice }) => {
  const dateValue = isPrice ? 'DD MMMM YYYY' : 'MMMM YYYY';
	return (
		<div className="sidebar-products__month-divider">
			<div className="sidebar-products__month-divider-date">
				{moment(date).format(dateValue)}
			</div>
			<div className="sidebar-products__month-divider-line"/>
		</div>
	);
};

export default MonthDivider;