import React, {useEffect, useState} from 'react';
import {Menu} from '../layouts/Main';
import classNames from 'classnames'
import {useHistory} from 'react-router-dom';

const GlobalNav = ({label, icon}) => {

  const icons = classNames('global-nav__item', {
    [`global-nav__item_${icon}`]: icon,
  });
  const history = useHistory();
  const onBackClick = () => history.goBack();

  return (
    <Menu
      isGlobalSearchPage
    >
      <div className="global-nav">
        <span onClick={onBackClick} className="global-nav__item global-nav__item_back">Назад</span>
        <span className="global-nav__slash">{` / `}</span>
        <span className={icons}>{label}</span>
      </div>
    </Menu>
  );
};

export default GlobalNav