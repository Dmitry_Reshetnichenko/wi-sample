import React, { useState } from 'react';
import classNames from 'classnames'
import SVG from 'react-inlinesvg';
import FavoriteIcon from '../../images/icons/favorites.svg';
import { addProductToFavourite, removeProductFromFavourite } from '../../store/actions/products'
import { connect } from 'react-redux';
import ProductHelper from '../../helpers/ProductHelper';

const Bookmark = (
  { isFavourite,
    addProductToFavourite,
    removeProductFromFavourite,
    user,
    roductId }) => {

  const [active, setVal] = useState(isFavourite);

  const onClick = event => {
    event.preventDefault();
    const favourite = {
      is_calendar: ProductHelper.isCalendar(),
      user: user._id,
      product: productId
    };

    active
      ? removeProductFromFavourite(favourite)
      : addProductToFavourite(favourite);

    setVal(!active);
  };

  const root = classNames('bookmark', {
    'bookmark_active': Boolean(active),
  });

  return (
    <div
      className={root}
      onClick={onClick}>
      <SVG src={FavoriteIcon} />
    </div>
  );
};

export default connect(
  state => ({
    user: state.auth.user
  }),
  dispatch => ({
    addProductToFavourite: favourite => dispatch(addProductToFavourite(favourite)),
    removeProductFromFavourite: filter => dispatch(removeProductFromFavourite(filter))
  })
)(Bookmark);