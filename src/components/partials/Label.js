import React from 'react';
import classNames from 'classnames'
import SVG from "react-inlinesvg";
import CoinIcon from "../../images/icons/header/coin.svg";

const Label = ({title, coinCurr, free, price, haveAccess}) => {

  const root = classNames('wi-label', {
    'wi-label_bg_grey': Boolean(free),
    'wi-label_bg_red': Boolean(price),
    'wi-label_style_access': Boolean(haveAccess),
  });
  return (
    <div className={root}>
      <span className="wi-label__text">
        {coinCurr && <SVG src={CoinIcon}/>}
        {title}</span>
    </div>
  );

};

export default Label