import React from 'react';
import LoaderIcon from '../../images/icons/loader.svg';
import classNames from 'classnames'

const Loader = (props) => {
	const loaderClasses = classNames('wi-loader', {
		'wi-loader_visible': Boolean(props.visible),

		'wi-loader_bg_black': Boolean(props.bgBlack),
		'wi-loader_bg_none': Boolean(props.bgNone),
		'wi-loader_bg_transparent': Boolean(props.bgTransparent),

		'wi-loader_fixed': Boolean(props.fixed),

		'wi-loader_size_xs': Boolean(props.xs),
		'wi-loader_size_sm': Boolean(props.sm),
		'wi-loader_size_md': Boolean(props.md),

		'wi-loader_top': Boolean(props.top)
	});
	return (
		<div className={loaderClasses}>
			<img className="wi-loader__icon" src={LoaderIcon} alt="loader"/>
		</div>
	);
}

export default Loader;