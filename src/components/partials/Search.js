import React, {useState} from "react";
import Button from '../../components/partials/Button';
import searchIcon from "../../images/icons/top-menu/search.svg";
import {connect} from "react-redux";
import {addSearchValue} from "../../store/actions/sidebarFilter";
import SVG from "react-inlinesvg";
import {useHistory} from 'react-router-dom';

const Search = ({addSearchValue, filter, isGlobalSearchPage}) => {

  const [inputValue, setInputValue] = useState(null);
  const history = useHistory();
  const handleInputChange = (e) => {
    e.preventDefault();
    const {value} = e.target;
    setInputValue(value);
  };

  const handleSearchSubmit = (e) => {
    e.preventDefault();

    if (inputValue) {
      addSearchValue(inputValue);
      if (!isGlobalSearchPage) {
        history.push('/global_search_results');
      }
    }
  };

  const placeHolderValue = (filter.searchValue && filter.searchValue.value) || 'Поиск';

  return (
    <form className={!isGlobalSearchPage ? "journal-menu__search" : "journal-menu__search-global-page"}
          onSubmit={handleSearchSubmit}>

      <Button className="wi-btn-property"
              type="submit"
      > <SVG src={searchIcon}/></Button>
      <input onChange={handleInputChange}
             name='search'
             className="journal-menu__search-input"
             placeholder={placeHolderValue}
      />
    </form>
  )
};

export default connect(
  state => ({
    filter: state.sidebarFilter.filter,
    pagination: state.products.pagination,
    sidebarFilter: state.sidebarFilter
  }),
  dispatch => ({
    addSearchValue: (value) => dispatch(addSearchValue(value)),
  })
)(Search);
