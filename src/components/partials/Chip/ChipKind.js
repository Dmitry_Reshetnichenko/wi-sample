import ProductTypes from "../../../constants/ProductTypes";
import ArticleIcon from "../../../images/icons/article.svg";
import VideoIcon from "../../../images/icons/video.svg";

const chipKind = {
  [ProductTypes.ARTICLE] : {
    title: 'Статья',
    icon: ArticleIcon,
  },
  [ProductTypes.VIDEO] : {
    title: 'Видео',
    icon: VideoIcon,
  },
  [ProductTypes.AUDIO] : {
    title: 'Аудио',
    icon: VideoIcon,
  },
  [ProductTypes.MOON_CALENDAR] : {
    title: 'Календарь',
    icon: VideoIcon,
  },
  [ProductTypes.WEBINAR] : {
    title: 'Трансляция',
    icon: VideoIcon,
  },
  [ProductTypes.OFFLINE_EVENT] : {
    title: 'Оффлайн',
    icon: VideoIcon,
  },
  [ProductTypes.COLLECTION] : {
    title: 'Марафон',
    icon: VideoIcon,
  }
};

export default chipKind