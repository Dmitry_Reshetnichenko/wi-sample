import React from 'react';
import classNames from 'classnames'
import SVG from 'react-inlinesvg';
import chipKind from './ChipKind'

const Chip = props => {
  const {ekind, courses} = props;
  const chip = ekind ? chipKind[ekind] : props;
  const title = chip && (!chip.bgDark && courses ? 'курсы' : chip.title);
  const root = classNames('wi-chip', {
    'wi-chip_bg_dark': chip && chip.bgDark,
  });

  return (
  	 <React.Fragment>
      {chip &&
				<div className={root}>
					{chip.icon && <SVG src={chip.icon} /> }
					<span className="wi-chip__text">{title}</span>
				</div>}
    </React.Fragment>
  );
};

export default Chip