import Published from '../../../images/icons/accept.svg';
import NotPublished from '../../../images/icons/non-published.svg';
import Denied from '../../../images/icons/denide.svg';
import OnModeration from '../../../images/icons/denide.svg';

const acceptStatuses = {
    'NotPublished': {
        title: 'не опубликованно',
        icon: NotPublished
    },
    'Published': {
        title: 'Опубликованно',
        icon: Published
    },
    'OnModeration': {
        title: 'На модерации',
        icon: OnModeration
    },
    'Denied': {
        title: 'Отказано',
        icon: Denied
    },
};

export default acceptStatuses