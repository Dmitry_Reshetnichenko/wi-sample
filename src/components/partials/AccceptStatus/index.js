import React from 'react';
import acceptStatuses from './AcceptStatuses'
import SVG from 'react-inlinesvg';

const AcceptStatus = ({status}) => {

    const statusItem = acceptStatuses[status]

    return (
      <div className='accept-status'>
          <SVG src={statusItem.icon}/>
          <span className='accept-status__title'>{statusItem.title}</span>
      </div>
    );
};

export default AcceptStatus