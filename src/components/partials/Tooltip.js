import React from 'react';
import classNames from "classnames";

const Tooltip = ({children, dataTip, topRight, topLeft, bottomLeft, bottomRight }) => {

  const root = classNames('wi-tooltip', {
    'ne': Boolean(topRight),
    'nw': Boolean(topLeft),
    'se': Boolean(bottomRight),
    'sw': Boolean(bottomLeft),
  });

  return (
    <div className={root} data-tooltip={dataTip}>
      {children}
    </div>
  );
};
export default Tooltip
