import React from 'react';


const AchievementGroup = ({children, title, description}) => {
	return (
		<div className="achievement-group">
			<div className="achievement-group__cards">
				{children}
			</div>
			<div className="achievement-group__info">
				<div className="achievement-group__title">{`«${title}»`}</div>
				<div className="achievement-group__description">{description}</div>
			</div>
		</div>
	);
};

export default AchievementGroup;