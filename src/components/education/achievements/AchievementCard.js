import React from 'react';
import classNames from 'classnames';
import {connect} from "react-redux";
import {achievementModalShow} from "../../../store/actions/modals";

const AchievementCard = (
  {active,
    large,
    images,
    label,
    clickable,
    name,
    achieved,
    showLabel,
    current_count,
    boundary_count,
    infinite}) => {

  const cardClasses = classNames('achievement-card', {
    'achievement-card_active': Boolean(active),
    'achievement-card_large': Boolean(large)
  });

  const getImg = () => {
    if (active) {
      return images.active[name]
    }
    if ( achieved || infinite) {
      return images.color[name]
    }
    return  images.grey[name]
  };

	const achievementPercent = ((boundary_count / 100) * current_count).toFixed();

  return (
    <div className={cardClasses} onClick={() => clickable && props.achievementModalShow(props)}>
      <div className="achievement-card__content">
        {current_count !== undefined && infinite
          ? <React.Fragment>
              <div className="achievement-card__progress-count">{current_count}</div>
            </React.Fragment>
          : <React.Fragment>
              <div className="achievement-card__progress-count">{achievementPercent}<i>%</i></div>
              <div className="achievement-card__progress">
                <div className="achievement-card__progress-fill"
                     style={{width: `${100 / boundary_count * current_count}%`}}/>
              </div>
            </React.Fragment>}
        <div className="achievement-card__image" style={{backgroundImage: `url(${getImg()})`}}/>
        {active && <div className="achievement-card__status">Выполняю</div>}
      </div>
      {showLabel && label && <div className="achievement-card__label">{`«${label}»`}</div>}
    </div>
  );
};
export default connect(
  state => ({
		images: state.achievements.images
	}),
  (dispatch) => ({
    achievementModalShow: (el) => dispatch(achievementModalShow(el)),
  })
)(AchievementCard);
