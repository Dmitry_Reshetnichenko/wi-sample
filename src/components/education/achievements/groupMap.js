 const groupMap = {
  greetings: {
    title: 'Добро пожаловать',
    description: 'Не забывайте про Сообщество. Помните, что когда вы тут - вы дома.'
  },
  knowledge: {
    title: 'Жажда знаний',
    description: 'Развитие - это непрерывный процесс. Заходите в приложение каждый день, обучайтесь и получайте награды.'
  },
  reading: {
    title: 'Чтение всему голова',
    description: 'Читайте, читайте и еще раз читайте. Каждая статья - это маленькая ступенька к счастливой жизни. Ну, и к награде.'
  },
  opinion: {
    title: 'Лидер мнений',
    description: 'Помогайте другим советом или делитесь своим мнением о просмотренном. Чем больше популярных комментариев, тем больше инсайтов и призов.'
  },
  talk: {
    title: 'Поговорим?',
    description: 'Создавайте новые темы для обсуждений в разделе Поддержка. Помогайте себе и другим найти ответы на волнующие вопросы.'
  },
  share: {
    title: 'Поделилась лучшим',
    description: 'Многие женщины задаются вопросом: почему у Машеньки есть всё, а я месяцами коплю на зимнюю '
  },
  invite_coin: {
    title: 'Сообщество подруг',
    description: 'Мы хотим донести ценность развития всем женщинам. Пригласи свою подругу в Сообщество WI и развивайтесь вместе на одной волне.'
  },
  invite_plan: {
    title: 'Подруги-кайфушницы',
    description: 'Расскажи приглашенной подруге про кайф Premium подписки и наслаждайтесь особенным контентом вдвоем.'
  }
};

export default groupMap