import React from 'react';
import { connect } from 'react-redux';
import AchievementCard from './AchievementCard';
import AchievementGroup from "./AchievementGroup";
import groupMap from "./groupMap";

const AchievementGroupList = ({ achievements }) => {
	return (
		<div className="achievements-other__list">
			{Object.values(achievements).map((item, index) =>
				<AchievementGroup key={index} {...groupMap[Object.keys(achievements)[index]]}>
					{item.map((i) => <AchievementCard key={i._id} clickable {...i} />)}
				</AchievementGroup>)}
		</div>
	);
};
export default connect(
	state => ({
		achievements: state.achievements.achievements
	})
)(AchievementGroupList)
