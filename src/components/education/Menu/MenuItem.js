import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import withSizes from 'react-sizes';
import Bubble from '../../partials/Bubble';

 const MenuItem = ({exact,bubbleNumber, label, path, isActive}) => {

	 return (
		 <NavLink to={path || '/'}
							isActive={isActive}
							exact={exact}
							className="journal-menu__nav-item"
							activeClassName="journal-menu__nav-item_active">
			 {bubbleNumber && <Bubble number={bubbleNumber} mobileEmpty/>}
			 <div className="journal-menu__nav-item-label" label={label}>{label}</div>
		 </NavLink>
	 );
};

const mapSizesToProps = ({width}) => ({
  isMobile: width < 768,
});

export default withSizes(mapSizesToProps)(MenuItem)