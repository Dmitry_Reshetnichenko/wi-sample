import React from 'react';
import {connect} from 'react-redux';
import MenuItem from './MenuItem';
import SectionNames from "../../../constants/SectionNames";

const EducationMenu = ({authorized, menuItems }) => {

  const ACHIEVEMENTS = `/${SectionNames.EDUCATION}/${SectionNames.ACHIEVEMENTS}`;
  const isAuthIMenuItems = authorized ? menuItems : menuItems.filter(i => ![ACHIEVEMENTS].includes(i.path));

  return (
    <div className="journal-menu">
      <div className="journal-menu__nav">
        {isAuthIMenuItems.map((item, index) =>
          <MenuItem key={index} {...item}/>
        )}
      </div>
    </div>
  );
}

export default connect(
  state => ({
    filterCount: state.sidebarFilter.count,
    authorized: state.auth.authorized
  })
)(EducationMenu);
