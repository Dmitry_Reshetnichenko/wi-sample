import React, {useState, useEffect, useRef} from 'react';
import SVG from 'react-inlinesvg';
import classNames from 'classnames';
import MenuItem from './MenuItem';
import dropDownIcon from '../../../images/icons/drop-down.svg'
import searchIcon from '../../../images/icons/top-menu/search.svg';

const menuItems = [
    {
        label: 'Статьи',
        bubbleNumber : 1,
        path: '/journal/articles',
        exact: true
    },
    {
        label: 'Видео',
        bubbleNumber : 2934,
        path: '/journal/video'
    },
    {
        label: 'Premium',
        bubbleNumber : 36,
        path: '/journal/premium'
    },
    {
        label: 'Призы',
        bubbleNumber : 222,
        path: '/journal/prizes'
    }
];


const MobileMenu = (props) => {

    const [active, setVal] = useState(false);
    const [title, setTitle] = useState(menuItems[0].label);

    const root = classNames('journal-title', {
        'journal-title_active': active,
    });

    const node = useRef();
    const handleClick = (e) => {
        if (node.current.contains(e.target)) {
            setVal(!active);
            return
        }
        setVal(active);
    };

    useEffect(() => {
        document.addEventListener("click", handleClick);
        return () => document.removeEventListener("click", handleClick);
    }, []);

    const getTitleId = (id) => {
      setTitle(menuItems[id].label)
    };

  return (
      <React.Fragment>
          <a href="#"
             className={root}
             ref={node}>
              {title}
          </a>
          <div className="drop-down-menu">
              <div className="drop-menu__nav">
                  {
                      menuItems.map((item, index) =>
                        <MenuItem key={index}
                                  id={index}
                                  getTitleId={getTitleId}
                                  {...item}
                        />)
                  }
              </div>
          </div>
      </React.Fragment>
    );
};

export default MobileMenu
