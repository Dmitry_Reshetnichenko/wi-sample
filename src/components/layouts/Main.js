import React, {useState} from 'react';
import classNames from 'classnames';
import {useScrollPosition} from "../../hooks/detectScrollPosition"

export const Container = props => {
  const containerClasses = classNames('main-layout', props.className, {
    'main-layout_nosidebar': Boolean(props.nosidebar)
  });
  return (
	 <div className={containerClasses}>
		 {props.children}
	 </div>
  );
};

export const Menu = props => {

  const [hideOnScroll, setHideOnScroll] = useState(true);

  const root = classNames('main-layout__menu', {
    'main-layout__menu_hide': !hideOnScroll,
  });

  const rootSearchPage = classNames('main-layout__menu-search-page', {
    'main-layout__menu_hide': !hideOnScroll,
  });

   useScrollPosition(({ prevPos, currPos }) => {
    const isShow = currPos.y < prevPos.y
    const isShow2 = currPos.y > prevPos.y

    if (isShow  && currPos.y < -182) setHideOnScroll(false)
    if (isShow2) setHideOnScroll(true)

  }, [hideOnScroll],
    false,
    false,
    300
  );

  return (
   <div className={props.isGlobalSearchPage ? rootSearchPage : root} >
     {props.children}
   </div>
  );
};

export const Sidebar = props => {
  const root = classNames('main-layout__sidebar', {
    'main-layout__sidebar_show': props.toggleSideBar,
  });

  const content = classNames('main-layout__sidebar-content', {
    'main-layout__sidebar-content_width main-layout__sidebar-content_scrollable': props.calendar,
  });

  return (
   <div className={root}>
     <div className={content}>
       {props.children}
     </div>
   </div>
  );
};

export const Content = props => {
  return (
   <div className="main-layout__content">
		 {props.children}
   </div>
  );
};
export const Page = props => {
  return (
   <div className="main-layout__page">
		 {props.children}
   </div>
  );
};
