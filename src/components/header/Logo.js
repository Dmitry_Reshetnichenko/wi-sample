import React from 'react';
import SVG from 'react-inlinesvg';
import { Link } from 'react-router-dom';
import logo from  '../../images/logotype.svg'

const Logo = () => {
  return (
    <Link to="/" className="header__logo"><SVG src={logo} /></Link>
  )
}

export default Logo;
