import React from 'react';
import MenuItem from './MenuItem';

// Icons
import JournalIcon from '../../../images/icons/24px/journal.svg';
import CommunityIcon from '../../../images/icons/24px/talks.svg';
import CalendarIcon from '../../../images/icons/24px/calendar.svg';
import ShopIcon from '../../../images/icons/24px/shop.svg';
import ProgressIcon from '../../../images/icons/header/progress.svg';

class Menu extends React.Component {
    state = {
        linkList: [
            {
                path: '/journal',
                label: 'Журнал',
                icon: JournalIcon,
                isActive: match => match || location.pathname === '/'
            },
            {
                path: '/calendar',
                label: 'Календарь',
                icon: CalendarIcon
            },
            {
                path: '/education',
                label: 'Развитие',
                icon: ProgressIcon,
                withAuth: false,
            },
            {
                path: '/communication',
                label: 'Общение',
                icon: CommunityIcon,
                // notificationCount: 2,
            },
            {
                path: 'http://wishop.club/collections/all?utm_source=journal&utm_medium=banner',
                label: 'Магазин',
                icon: ShopIcon,
                outLink: true
            }
        ]
    };
    render(){
        return (
            <nav className="header__menu">
                {this.state.linkList.map((link, index) => (<MenuItem key={index} {...link} />))}
            </nav>
        )
    };
}

export default Menu;
