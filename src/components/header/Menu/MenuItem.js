import React, {Component} from 'react';
import {NavLink} from 'react-router-dom';
import SVG from 'react-inlinesvg';
import Bubble from '../../partials/Bubble';
import {connect} from "react-redux";

const MenuItem = (
  {
    outLink,
    path,
    isActive,
    notificationCount,
    icon,
    label,
    withAuth,
    isAuth
  }) => {

  if (!isAuth && withAuth) {
    return null
  }

  return (
    outLink
      ? <a className="header__menu-item" href={path} target="_blank">
        <div className="header__menu-item-icon">
          <SVG src={icon}/>
          {notificationCount && <Bubble number={notificationCount} forIcon/>}
        </div>
        <span className="header__menu-item-label">{label}</span>
      </a>
      : <NavLink to={path} isActive={isActive} className="header__menu-item" activeClassName="header__menu-item_active">
        <div className="header__menu-item-icon">
          <SVG src={icon}/>
          {notificationCount && <Bubble number={notificationCount} forIcon/>}
        </div>
        <span className="header__menu-item-label">{label}</span>
      </NavLink>
  )
};


export default connect(
  state => ({
    isAuth: state.auth.user
  })
)(MenuItem);
