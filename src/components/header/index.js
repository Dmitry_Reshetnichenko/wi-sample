import React from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';

import '../../sass/main.scss';
import Logo from  './Logo'
import Menu from './Menu'
import Widget from './Widget'
import withSizes from 'react-sizes'

import {toggleSideBar} from "../../store/actions/sidebarFilter";

const Header = () => {

  return (
    <header className="header" >
      <Logo/>
      <Menu />
      <Widget />
    </header>
  )
}

const mapSizesToProps = ({ width }) => ({
    isMobile: width < 768,
});

export default  compose(
  withSizes(mapSizesToProps),
  connect(
    state => ({
      authorized: state.auth.authorized
    }),
    dispatch => ({
      toggleSideBar: () => dispatch(toggleSideBar())
    }))
)(Header)
