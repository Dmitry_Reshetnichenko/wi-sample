import * as React from 'react';
import SVG from "react-inlinesvg";
import BellIcon from "../../../images/icons/bell.svg";
import NotificationMenu from "./NotificationMenu";
import classNames from 'classnames'
import {connect} from "react-redux";
import { Link } from 'react-router-dom';

const Notice = (props) => {
  const root = classNames('header__widget-authenticated-menu-notification', {
    'header__widget-authenticated-menu-notification_mobile': props.mobile
  });

  const NoticeBody = <div className="header__widget-authenticated-menu-notification-icon">
    <SVG src={BellIcon}/>
    <div
      className="header__widget-authenticated-menu-notification-icon-counter">
      {props.unreadNotifications > 9 ? '9+' : props.unreadNotifications}
    </div>
  </div>;

  return (
    <React.Fragment>
      {props.mobile ?
        <Link to='/notifications' className={root}>
          {NoticeBody}
        </Link>
        : <div className={root}>
          {NoticeBody}
          {!props.mobile && <NotificationMenu/>}
        </div>
      }
    </React.Fragment>
  );
};

export default connect(
  state => ({
    unreadNotifications: state.notifications.unreadNotifications
  })
)(Notice);