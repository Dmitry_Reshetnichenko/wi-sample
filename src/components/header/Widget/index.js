import React from 'react';
import { connect } from 'react-redux';
import Authenticated from './Authenticated'
import Unauthenticated from './Unauthenticated'
import Notice from "./Notice";

const Widget = ({authorized}) => {
  return (
    <div className="header__widget">
      {authorized
        ? <React.Fragment>
          <Notice mobile/>
          <Authenticated/>
        </React.Fragment>
        : <Unauthenticated/>}
    </div>
  )
};

export default connect(
    state => ({
      authorized: state.auth.authorized
    })
)(Widget);
