import React, {useRef} from 'react';
import SVG from 'react-inlinesvg';
import LogoutIcon from '../../../images/icons/16px/logout.svg';
import {connect} from 'react-redux';
import {compose} from 'redux';
import withSizes from 'react-sizes'
import classNames from 'classnames';
import {Link} from 'react-router-dom';

import {logout} from '../../../store/actions/auth';
import Loader from "../../partials/Loader";
import closeIcon from '../../../images/icons/cross.svg'
import arrowRightIcon from '../../../images/icons/menu-right-arrow.svg'
import personIcon from '../../../images/icons/person.svg'
import walletIcon from '../../../images/icons/walet.svg'
import mailIcon from '../../../images/icons/mail.svg'
import inviteIcon from '../../../images/icons/profile/profile-invites.svg';
import MoreDetailsIcon from '../../../images/icons/profile/more-details.svg'

const DropDownMenu = (props) => {
  const root = classNames('header__widget-authenticated-menu-dropdown-list', {
    'header__widget-authenticated-menu-dropdown-list_show': props.showDropDown,
  });
  const fullName = `${props.user.first_name || 'Безимянный'} ${props.user.last_name || ''}`
  const avatar = `${process.env.IMAGE_URL}${props.user.photo || '/images/avatars/defaultAvatar.jpg'}`;
  return (
    <React.Fragment>
      {
        props.isMobile ?
          <div className={root}>
            <a href='#' onClick={() => props.handleClick()}><SVG src={closeIcon}/></a>
            <div className="header__widget-authenticated"
                 style={{position: 'static', paddingTop: '32px', marginBottom: '44px'}}>
              <Loader bgBlack visible={props.loading}/>
              <div className="header__widget-authenticated-user">
                <div className="wi-ava wi-ava_lg">
                  <img className="wi-ava__img"
                       src={avatar} alt=""/>
                </div>
                <div className="header__widget-authenticated-user-info">
                  <div className="header__widget-authenticated-user-name">Добро пожаловать {fullName}</div>
                  <div
                    className="header__widget-authenticated-user-status header__widget-authenticated-user-status--gold-user">статус:
                    <span className="header__widget-authenticated-user-status-value">{props.status}</span>
                  </div>
                  <span className="header__widget-authenticated-menu-balance-value">{props.balance}</span>
                </div>
              </div>
            </div>
            <div className="header__widget-authenticated-menu-dropdown-list-corner"/>
            <Link to="/profile/info" onClick={() => props.handleClick()}
                  className="header__widget-authenticated-menu-dropdown-list-item">
              <SVG src={personIcon}/><span className="header__widget-authenticated-menu-dropdown-list-text">Личные данные</span>
              <SVG src={arrowRightIcon}/>
            </Link>
            <Link to="/profile/payments" onClick={() => props.handleClick()}
                  className="header__widget-authenticated-menu-dropdown-list-item">
              <SVG src={walletIcon}/><span className="header__widget-authenticated-menu-dropdown-list-text">Платежные данные</span><SVG
              src={arrowRightIcon}/>
            </Link>
            <Link to="/profile/plans" onClick={() => props.handleClick()}
                  className="header__widget-authenticated-menu-dropdown-list-item">
              <SVG src={mailIcon}/><span
              className="header__widget-authenticated-menu-dropdown-list-text">Планы подписок</span><SVG
              src={arrowRightIcon}/>
            </Link>
            <Link to="/profile/invites" onClick={() => props.handleClick()}
                  className="header__widget-authenticated-menu-dropdown-list-item">
              <SVG src={inviteIcon}/><span
              className="header__widget-authenticated-menu-dropdown-list-text">Приглашения</span><SVG
              src={arrowRightIcon}/>
            </Link>
            <Link to="/profile/support" onClick={() => props.handleClick()}
                  className="header__widget-authenticated-menu-dropdown-list-item">
              <SVG src={MoreDetailsIcon}/><span
              className="header__widget-authenticated-menu-dropdown-list-text">Служба поддержки</span><SVG
              src={arrowRightIcon}/>
            </Link>
            <Link to="/journal/articles/5e2f14cf6137d03aea4e7957" onClick={() => props.handleClick()}
                  className="header__widget-authenticated-menu-dropdown-list-item">
              <SVG src={MoreDetailsIcon}/><span
              className="header__widget-authenticated-menu-dropdown-list-text">FAQ</span><SVG
              src={arrowRightIcon}/>
            </Link>
            {/*<li className="header__widget-authenticated-menu-dropdown-list-item"><SVG src={personIcon}/><span>Служба поддержки</span><SVG src={arrowRightIcon}/></li>*/}
            <div className="header__widget-authenticated-menu-dropdown-list-separator"/>
            <button type="button" className="wi-btn wi-btn__link header__widget-authenticated-menu-dropdown-list-item"
                    onClick={props.logout}>
              <SVG src={LogoutIcon}/>
              <span className="header__widget-authenticated-menu-dropdown-list-text">Выйти</span>
            </button>
          </div>
          :
          <div className={root}>
            <div className="header__widget-authenticated-menu-dropdown-list-corner"/>
            <Link to="/profile/info" data-link className="header__widget-authenticated-menu-dropdown-list-item">Личные
              данные</Link>
            <Link to="/profile/payments" data-link className="header__widget-authenticated-menu-dropdown-list-item">Платежные
              данные</Link>
            <Link to="/profile/plans" data-link className="header__widget-authenticated-menu-dropdown-list-item">Планы
              подписок</Link>
            <Link to="/profile/invites" data-link className="header__widget-authenticated-menu-dropdown-list-item">Приглашения</Link>
            <Link to="/profile/support" data-link className="header__widget-authenticated-menu-dropdown-list-item">Служба поддержки</Link>
            <Link to="/journal/articles/5e2f14cf6137d03aea4e7957" data-link className="header__widget-authenticated-menu-dropdown-list-item">FAQ</Link>
            <div className="header__widget-authenticated-menu-dropdown-list-separator"/>
            <button type="button" className="wi-btn wi-btn__link header__widget-authenticated-menu-dropdown-list-item"
                    onClick={props.logout}>
              <SVG src={LogoutIcon}/>
              <span className="header__widget-authenticated-menu-dropdown-list-text">Выйти</span>
            </button>
          </div>
      }

    </React.Fragment>
  )
};

const mapSizesToProps = ({width}) => ({
  isMobile: width < 768,
});


export default compose(
  withSizes(mapSizesToProps),
  connect(
    state => ({
      user: state.auth.user || {},
      loading: state.auth.loading
    }),
    dispatch => ({
      logout: () => dispatch(logout())
    })
  ))(DropDownMenu);
