import React from "react";
import {Link} from 'react-router-dom';
import NotificationsActionsBar from "../../partials/Notification/NotificationsActionsBar";
import NotificationList from "../../partials/Notification/NotificationList";
import EmptyState from "../../partials/EmptyState";
import {connect} from "react-redux";
import Loader from '../../../components/partials/Loader';
import NotificationEmptyState from "../../../images/icons/empty-states/notification-empty-state.svg";

const NotificationMenu = ({notifications, notificationsLoading}) => {

  const menuContent = !notifications.length && !notificationsLoading
    ? <EmptyState icon={NotificationEmptyState}/>
    : <React.Fragment>
        <NotificationsActionsBar/>
        <NotificationList notifications={notifications} menu/>
        <div className="notification-menu__footer">
          <Link className="wi-btn wi-btn__link wi-btn__link_cl_primary" to='/notifications'>Смотреть все</Link>
        </div>
      </React.Fragment>;

  return (
    <div className="notification-menu">
      <Loader visible={Boolean(notificationsLoading)}/>
      {menuContent}
    </div>
  )
};

export default connect(
  state => ({
    notifications: state.notifications.notifications,
    notificationsLoading: state.notifications.loading
  })
)(NotificationMenu);

