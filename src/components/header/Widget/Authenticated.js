import React, {useEffect, useState, useRef} from 'react';
import {compose} from 'redux';
import {connect} from 'react-redux';
import DropDownMenu from './DropDownMenu';
import Loader from '../../partials/Loader';
import classNames from 'classnames';
import {Link} from 'react-router-dom';
import Notice from './Notice'
import moment from 'moment-timezone';
import {getNotifications, getNewNotificationsCount} from "../../../store/actions/notifications";

const Authenticated = props => {
  const root =  classNames('header__widget-authenticated-user-status',
    {'header__widget-authenticated-user-status--gold-user': props.user.goldUser})

  const [dropDownShow, setDropDownShow] = useState(false);
  const [dropDownShowMobile, setDropDownShowMobile] = useState(false);
  const node = useRef();
  const avaNode = useRef();

  const handleClick = (e) => {
    if ((node || avaNode) &&
      (node.current.contains(e.target) && !e.target.href) ||
      (avaNode.current.contains(e.target) && !e.target.href)) {
      setDropDownShow(!dropDownShow);
      return
    }
    setDropDownShow(dropDownShow);
  };

  useEffect(() => {
    props.user._id && !props.notificationsLoading && props.getNotifications({filter: {user: props.user._id}});
    props.user._id && !props.notificationsLoading && props.getNewNotificationsCount({filter: {user: props.user._id, is_read: false}});
    document.addEventListener("click", handleClick);
    return () => document.removeEventListener("click", handleClick);
  }, [props.user._id]);

  const openMobileMenu = () => {
    document.body.style.overflow = "hidden";
    setDropDownShowMobile(true);
  };

  const closeMobileMenu = () => {
    setDropDownShowMobile(false);
    document.body.style.overflow = "auto";
  };

  const userMembership =
    props.activeMemberships
      .sort((a, b) =>
        moment().add(b.bill_period.interval_count, b.bill_period.interval)
          .diff(moment().add(a.bill_period.interval_count, a.bill_period.interval))
      )[0];
  
  const fullName = `${props.user.first_name || 'Безимянный'} ${props.user.last_name || ''}`;
  const avatar = `${process.env.IMAGE_URL}${props.user.photo || '/images/avatars/defaultAvatar.jpg'}`;

  const plan = props.plans.find(i => i._id === (userMembership && userMembership.plan));
  const status = props.user.goldUser ? plan && plan.title || 'Premium' : 'Free User';
  const balance = props.user.balance || 0;

  return (
    <React.Fragment>
      <div className="header__widget-authenticated desktop-hide">
        <Loader bgBlack visible={props.loading}/>
        <div className="header__widget-authenticated-user">
          <div className="wi-ava" onClick={() => openMobileMenu()}>
            <img className="wi-ava__img" src={avatar} alt=""/>
          </div>
        </div>
        <DropDownMenu showDropDown={dropDownShowMobile} handleClick={closeMobileMenu} status={status}  balance={balance}/>
      </div>

      <div className="header__widget-authenticated mobile-hide">
        <Loader bgBlack visible={props.loading}/>
        <div className="header__widget-authenticated-container">
          <div>
            <div className="wi-ava wi-ava_lg" ref={avaNode}>
              <img className="wi-ava__img" src={avatar} alt=""/>
            </div>
          </div>
          <div>
            <div className="header__widget-authenticated-user">
              <div className="header__widget-authenticated-user-info">
                <div className="header__widget-authenticated-user-name">{fullName}</div>
                <div className={root}>Premium:<span className="header__widget-authenticated-user-status-value">{status}</span>
                </div>
              </div>
            </div>
            <div className="header__widget-authenticated-menu plug">
              <Notice />
              <div className="header__widget-authenticated-menu-separator"/>
              <Link to='#' className="header__widget-authenticated-menu-balance">
                <span className="header__widget-authenticated-menu-balance-value">{balance}</span>
              </Link>
              <div className="header__widget-authenticated-menu-separator"/>
              <div className="header__widget-authenticated-menu-dropdown">
                <div className="header__widget-authenticated-menu-dropdown-button" ref={node}>
                  <button type="button" className="wi-btn-property"><SVG src={dotsIcon}/></button>
                  <DropDownMenu showDropDown={dropDownShow}/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>

  )
};

export default compose(
  connect(
    state => ({
      plans: state.global.plans,
      user: state.auth.user || {},
      activeMemberships: state.plans.activeMembership,
      loading: state.auth.loading,
      notificationsLoading: state.notifications.notifications.loading,
      unreadNotifications: state.notifications.unreadNotifications
      
    }),
    dispatch => ({
      getNotifications: (filter) => dispatch(getNotifications(filter)),
      getNewNotificationsCount: (filter) => dispatch(getNewNotificationsCount(filter))
    })
  ))(Authenticated);
