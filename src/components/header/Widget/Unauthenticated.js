import React from 'react';
import {LOGIN, REGISTER} from '../../modals/AuthModal/TabTypes';
import {connect} from 'react-redux';
import {openModal} from '../../../store/actions/auth';
import Button from '../../partials/Button';

const Unauthenticated = ({openModal}) => {
  return (
    <React.Fragment>
      <div className="header__widget-unauthenticated">
        <Button
          className="header__widget-unauthenticated-login-btn"
          sm
          dark
          to={{search: `?modalTab=${LOGIN}`}}
          onClick={() => openModal(LOGIN)}
        >
          Вход
        </Button>
        <Button
          className="header__widget-unauthenticated-register-btn"
          sm
          to={{search: `?modalTab=${REGISTER}`}}
          onClick={() => openModal(REGISTER)}
        >
          Регистрация
        </Button>
      </div>
    </React.Fragment>
  )
};


export default connect(
  null,
  dispatch => ({
    openModal: modalTab => dispatch(openModal(modalTab)),
  })
)(Unauthenticated);
